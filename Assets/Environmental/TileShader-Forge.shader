// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:31822,y:32933|diff-257-OUT,normal-245-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:2,x:32866,y:32519;n:type:ShaderForge.SFN_ChannelBlend,id:3,x:31659,y:33433,chbt:0;n:type:ShaderForge.SFN_Tex2d,id:4,x:32718,y:32989,ptlb:low_flat_tex,ptin:_low_flat_tex,tex:2cfa2c8ec0c33994ebb7826566810870,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5,x:32883,y:32989,ptlb:low_flat_bump,ptin:_low_flat_bump,tex:1247b1a99609d434fb2d39ec9a3b9a17,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:6,x:32718,y:32804,ptlb:high_flat_tex,ptin:_high_flat_tex,tex:865c4f1a5b85a634281e3d94df740fa0,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7,x:32883,y:32804,ptlb:high_flat_bump,ptin:_high_flat_bump,tex:eebab8d8deb767c45bd7574502b529de,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:8,x:33042,y:32989,ptlb:low_slope_tex,ptin:_low_slope_tex,tex:3a6c89839baf7e3408e096c1ed484366,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9,x:33202,y:32989,ptlb:low_slope_bump,ptin:_low_slope_bump,tex:84474966fb2229e45a4fc13a9d55ae07,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Tex2d,id:10,x:33042,y:32804,ptlb:high_slope_tex,ptin:_high_slope_tex,tex:3b62fd15386993f4fa1734d1a18f4691,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:11,x:33202,y:32804,ptlb:high_slope_bump,ptin:_high_slope_bump,tex:16c63826d9f608846acda6c68f49a3fb,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Blend,id:12,x:32202,y:32719,blmd:10,clmp:True;n:type:ShaderForge.SFN_Slider,id:13,x:32492,y:32622,ptlb:min_altitude,ptin:_min_altitude,min:0,cur:0,max:50;n:type:ShaderForge.SFN_Slider,id:14,x:32492,y:32525,ptlb:max_altitude,ptin:_max_altitude,min:0,cur:1,max:50;n:type:ShaderForge.SFN_Slider,id:15,x:32550,y:33646,ptlb:max_slope,ptin:_max_slope,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:16,x:32510,y:32869|A-4-RGB,B-6-RGB,T-213-OUT;n:type:ShaderForge.SFN_Lerp,id:32,x:32510,y:33006|A-5-RGB,B-7-RGB,T-213-OUT;n:type:ShaderForge.SFN_Lerp,id:48,x:32510,y:33144|A-8-RGB,B-10-RGB,T-213-OUT;n:type:ShaderForge.SFN_Lerp,id:49,x:32525,y:33282|A-11-RGB,B-9-RGB,T-213-OUT;n:type:ShaderForge.SFN_NormalVector,id:157,x:32732,y:33423,pt:False;n:type:ShaderForge.SFN_ChannelBlend,id:158,x:32480,y:32722,chbt:0;n:type:ShaderForge.SFN_ComponentMask,id:159,x:32525,y:33437,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-157-OUT;n:type:ShaderForge.SFN_Lerp,id:160,x:32283,y:32914|A-48-OUT,B-16-OUT,T-265-OUT;n:type:ShaderForge.SFN_Vector1,id:173,x:32369,y:33540,v1:2;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:181,x:33171,y:32642|IN-2-Y,IMIN-13-OUT,IMAX-14-OUT,OMIN-183-OUT,OMAX-184-OUT;n:type:ShaderForge.SFN_RemapRange,id:182,x:33093,y:32354,frmn:0,frmx:1,tomn:-1,tomx:1;n:type:ShaderForge.SFN_Vector1,id:183,x:33046,y:32529,v1:0;n:type:ShaderForge.SFN_Vector1,id:184,x:33195,y:32507,v1:1;n:type:ShaderForge.SFN_Power,id:195,x:32997,y:32642|VAL-181-OUT,EXP-201-OUT;n:type:ShaderForge.SFN_Vector1,id:201,x:32692,y:32734,v1:0.7;n:type:ShaderForge.SFN_Clamp01,id:213,x:32840,y:32659|IN-195-OUT;n:type:ShaderForge.SFN_RemapRange,id:224,x:32342,y:33348,frmn:0,frmx:1,tomn:-1,tomx:1|IN-159-OUT;n:type:ShaderForge.SFN_Lerp,id:245,x:32283,y:33069|A-49-OUT,B-32-OUT,T-265-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:256,x:31827,y:33349;n:type:ShaderForge.SFN_RemapRange,id:257,x:32100,y:32919,frmn:0,frmx:1,tomn:0,tomx:1.5|IN-160-OUT;n:type:ShaderForge.SFN_Power,id:265,x:32165,y:33394|VAL-224-OUT,EXP-173-OUT;proporder:4-6-5-7-8-10-9-11-13-14;pass:END;sub:END;*/

Shader "Shader Forge/TileShader-Forge" {
    Properties {
        _low_flat_tex ("low_flat_tex", 2D) = "white" {}
        _high_flat_tex ("high_flat_tex", 2D) = "white" {}
        _low_flat_bump ("low_flat_bump", 2D) = "bump" {}
        _high_flat_bump ("high_flat_bump", 2D) = "bump" {}
        _low_slope_tex ("low_slope_tex", 2D) = "white" {}
        _high_slope_tex ("high_slope_tex", 2D) = "white" {}
        _low_slope_bump ("low_slope_bump", 2D) = "bump" {}
        _high_slope_bump ("high_slope_bump", 2D) = "bump" {}
        _min_altitude ("min_altitude", Range(0, 50)) = 0
        _max_altitude ("max_altitude", Range(0, 50)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _low_flat_tex; uniform float4 _low_flat_tex_ST;
            uniform sampler2D _low_flat_bump; uniform float4 _low_flat_bump_ST;
            uniform sampler2D _high_flat_tex; uniform float4 _high_flat_tex_ST;
            uniform sampler2D _high_flat_bump; uniform float4 _high_flat_bump_ST;
            uniform sampler2D _low_slope_tex; uniform float4 _low_slope_tex_ST;
            uniform sampler2D _low_slope_bump; uniform float4 _low_slope_bump_ST;
            uniform sampler2D _high_slope_tex; uniform float4 _high_slope_tex_ST;
            uniform sampler2D _high_slope_bump; uniform float4 _high_slope_bump_ST;
            uniform float _min_altitude;
            uniform float _max_altitude;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_273 = i.uv0;
                float node_183 = 0.0;
                float node_213 = saturate(pow((node_183 + ( (i.posWorld.g - _min_altitude) * (1.0 - node_183) ) / (_max_altitude - _min_altitude)),0.7));
                float node_265 = pow((i.normalDir.g*2.0+-1.0),2.0);
                float3 normalLocal = lerp(lerp(UnpackNormal(tex2D(_high_slope_bump,TRANSFORM_TEX(node_273.rg, _high_slope_bump))).rgb,UnpackNormal(tex2D(_low_slope_bump,TRANSFORM_TEX(node_273.rg, _low_slope_bump))).rgb,node_213),lerp(UnpackNormal(tex2D(_low_flat_bump,TRANSFORM_TEX(node_273.rg, _low_flat_bump))).rgb,UnpackNormal(tex2D(_high_flat_bump,TRANSFORM_TEX(node_273.rg, _high_flat_bump))).rgb,node_213),node_265);
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.rgb;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (lerp(lerp(tex2D(_low_slope_tex,TRANSFORM_TEX(node_273.rg, _low_slope_tex)).rgb,tex2D(_high_slope_tex,TRANSFORM_TEX(node_273.rg, _high_slope_tex)).rgb,node_213),lerp(tex2D(_low_flat_tex,TRANSFORM_TEX(node_273.rg, _low_flat_tex)).rgb,tex2D(_high_flat_tex,TRANSFORM_TEX(node_273.rg, _high_flat_tex)).rgb,node_213),node_265)*1.5+0.0);
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _low_flat_tex; uniform float4 _low_flat_tex_ST;
            uniform sampler2D _low_flat_bump; uniform float4 _low_flat_bump_ST;
            uniform sampler2D _high_flat_tex; uniform float4 _high_flat_tex_ST;
            uniform sampler2D _high_flat_bump; uniform float4 _high_flat_bump_ST;
            uniform sampler2D _low_slope_tex; uniform float4 _low_slope_tex_ST;
            uniform sampler2D _low_slope_bump; uniform float4 _low_slope_bump_ST;
            uniform sampler2D _high_slope_tex; uniform float4 _high_slope_tex_ST;
            uniform sampler2D _high_slope_bump; uniform float4 _high_slope_bump_ST;
            uniform float _min_altitude;
            uniform float _max_altitude;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_274 = i.uv0;
                float node_183 = 0.0;
                float node_213 = saturate(pow((node_183 + ( (i.posWorld.g - _min_altitude) * (1.0 - node_183) ) / (_max_altitude - _min_altitude)),0.7));
                float node_265 = pow((i.normalDir.g*2.0+-1.0),2.0);
                float3 normalLocal = lerp(lerp(UnpackNormal(tex2D(_high_slope_bump,TRANSFORM_TEX(node_274.rg, _high_slope_bump))).rgb,UnpackNormal(tex2D(_low_slope_bump,TRANSFORM_TEX(node_274.rg, _low_slope_bump))).rgb,node_213),lerp(UnpackNormal(tex2D(_low_flat_bump,TRANSFORM_TEX(node_274.rg, _low_flat_bump))).rgb,UnpackNormal(tex2D(_high_flat_bump,TRANSFORM_TEX(node_274.rg, _high_flat_bump))).rgb,node_213),node_265);
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (lerp(lerp(tex2D(_low_slope_tex,TRANSFORM_TEX(node_274.rg, _low_slope_tex)).rgb,tex2D(_high_slope_tex,TRANSFORM_TEX(node_274.rg, _high_slope_tex)).rgb,node_213),lerp(tex2D(_low_flat_tex,TRANSFORM_TEX(node_274.rg, _low_flat_tex)).rgb,tex2D(_high_flat_tex,TRANSFORM_TEX(node_274.rg, _high_flat_tex)).rgb,node_213),node_265)*1.5+0.0);
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
