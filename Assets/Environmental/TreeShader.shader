// Shader created with Shader Forge Beta 0.36 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.36;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:True,rmgx:True,rpth:1,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:0,culm:0,dpts:2,wrdp:True,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:1,x:33098,y:32820|diff-2-RGB,normal-8-RGB,voffset-124-OUT;n:type:ShaderForge.SFN_Tex2d,id:2,x:33454,y:32741,ptlb:main_texture,ptin:_main_texture,tex:dfcc518b752253446a7b63e3800c5f3d,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:8,x:33434,y:32964,ptlb:main_normal,ptin:_main_normal,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Vector4Property,id:14,x:33839,y:33301,ptlb:offset_1,ptin:_offset_1,glob:False,v1:-0.4,v2:0.1,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4Property,id:31,x:34073,y:33301,ptlb:offset_2,ptin:_offset_2,glob:False,v1:-0.3,v2:0.2,v3:0,v4:0;n:type:ShaderForge.SFN_Vector4Property,id:32,x:34125,y:33117,ptlb:offset_3,ptin:_offset_3,glob:False,v1:-0.1,v2:-0.1,v3:0,v4:0;n:type:ShaderForge.SFN_Multiply,id:34,x:34397,y:32927|A-58-UVOUT,B-35-OUT;n:type:ShaderForge.SFN_Vector1,id:35,x:34575,y:33061,v1:20;n:type:ShaderForge.SFN_Frac,id:36,x:34226,y:32882|IN-34-OUT;n:type:ShaderForge.SFN_If,id:37,x:33657,y:33144|A-44-R,B-43-OUT,GT-45-OUT,EQ-14-XYZ,LT-14-XYZ;n:type:ShaderForge.SFN_Vector1,id:43,x:33817,y:33054,v1:0.3333;n:type:ShaderForge.SFN_ComponentMask,id:44,x:34056,y:32872,cc1:0,cc2:1,cc3:1,cc4:-1|IN-36-OUT;n:type:ShaderForge.SFN_If,id:45,x:33839,y:33130|A-44-G,B-48-OUT,GT-32-XYZ,EQ-31-XYZ,LT-31-XYZ;n:type:ShaderForge.SFN_Vector1,id:48,x:34112,y:33021,v1:0.5;n:type:ShaderForge.SFN_ComponentMask,id:50,x:33497,y:33144,cc1:0,cc2:1,cc3:2,cc4:-1|IN-37-OUT;n:type:ShaderForge.SFN_TexCoord,id:58,x:34575,y:32885,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:84,x:33485,y:33330,ptlb:tree_size,ptin:_tree_size,glob:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:85,x:33319,y:33144|A-50-OUT,B-84-OUT;n:type:ShaderForge.SFN_Cross,id:124,x:33644,y:32891|A-132-OUT,B-85-OUT;n:type:ShaderForge.SFN_NormalVector,id:132,x:33820,y:32720,pt:False;proporder:2-8-14-31-32-84;pass:END;sub:END;*/

Shader "Shader Forge/TreeShader" {
    Properties {
        _main_texture ("main_texture", 2D) = "black" {}
        _main_normal ("main_normal", 2D) = "bump" {}
        _offset_1 ("offset_1", Vector) = (-0.4,0.1,0,0)
        _offset_2 ("offset_2", Vector) = (-0.3,0.2,0,0)
        _offset_3 ("offset_3", Vector) = (-0.1,-0.1,0,0)
        _tree_size ("tree_size", Float ) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "PrePassBase"
            Tags {
                "LightMode"="PrePassBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_PREPASSBASE
            #include "UnityCG.cginc"
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform fixed4 unity_Ambient;
            uniform sampler2D _main_normal; uniform float4 _main_normal_ST;
            uniform float4 _offset_1;
            uniform float4 _offset_2;
            uniform float4 _offset_3;
            uniform float _tree_size;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 node_44 = frac((o.uv0.rg*20.0)).rgg;
                float node_37_if_leA = step(node_44.r,0.3333);
                float node_37_if_leB = step(0.3333,node_44.r);
                float node_45_if_leA = step(node_44.g,0.5);
                float node_45_if_leB = step(0.5,node_44.g);
                v.vertex.xyz += cross(v.normal,(lerp((node_37_if_leA*_offset_1.rgb)+(node_37_if_leB*lerp((node_45_if_leA*_offset_2.rgb)+(node_45_if_leB*_offset_3.rgb),_offset_2.rgb,node_45_if_leA*node_45_if_leB)),_offset_1.rgb,node_37_if_leA*node_37_if_leB).rgb*_tree_size));
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_142 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_main_normal,TRANSFORM_TEX(node_142.rg, _main_normal))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                return fixed4( normalDirection * 0.5 + 0.5, max(0.5,0.0078125) );
            }
            ENDCG
        }
        Pass {
            Name "PrePassFinal"
            Tags {
                "LightMode"="PrePassFinal"
            }
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_PREPASSFINAL
            #include "UnityCG.cginc"
            #pragma multi_compile_prepassfinal
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform sampler2D _LightBuffer;
            #if defined (SHADER_API_XBOX360) && defined (HDR_LIGHT_PREPASS_ON)
                sampler2D _LightSpecBuffer;
            #endif
            uniform fixed4 unity_Ambient;
            uniform sampler2D _main_texture; uniform float4 _main_texture_ST;
            uniform sampler2D _main_normal; uniform float4 _main_normal_ST;
            uniform float4 _offset_1;
            uniform float4 _offset_2;
            uniform float4 _offset_3;
            uniform float _tree_size;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                float4 projPos : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 node_44 = frac((o.uv0.rg*20.0)).rgg;
                float node_37_if_leA = step(node_44.r,0.3333);
                float node_37_if_leB = step(0.3333,node_44.r);
                float node_45_if_leA = step(node_44.g,0.5);
                float node_45_if_leB = step(0.5,node_44.g);
                v.vertex.xyz += cross(v.normal,(lerp((node_37_if_leA*_offset_1.rgb)+(node_37_if_leB*lerp((node_45_if_leA*_offset_2.rgb)+(node_45_if_leB*_offset_3.rgb),_offset_2.rgb,node_45_if_leA*node_45_if_leB)),_offset_1.rgb,node_37_if_leA*node_37_if_leB).rgb*_tree_size));
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.projPos = ComputeScreenPos (o.pos);
                COMPUTE_EYEDEPTH(o.projPos.z);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_143 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_main_normal,TRANSFORM_TEX(node_143.rg, _main_normal))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
////// Lighting:
                half4 lightAccumulation = tex2Dproj(_LightBuffer, UNITY_PROJ_COORD(i.projPos));
                #if defined (SHADER_API_GLES) || defined (SHADER_API_GLES3)
                    lightAccumulation = max(lightAccumulation, half4(0.001));
                #endif
                #ifndef HDR_LIGHT_PREPASS_ON
                    lightAccumulation = -log2(lightAccumulation);
                #endif
                #if defined (SHADER_API_XBOX360) && defined (HDR_LIGHT_PREPASS_ON)
                    lightAccumulation.w = tex2Dproj (_LightSpecBuffer, UNITY_PROJ_COORD(i.projPos)).r;
                #endif
/////// Diffuse:
                float3 diffuse = lightAccumulation.rgb + unity_Ambient.rgb;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * tex2D(_main_texture,TRANSFORM_TEX(node_143.rg, _main_texture)).rgb;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _main_texture; uniform float4 _main_texture_ST;
            uniform sampler2D _main_normal; uniform float4 _main_normal_ST;
            uniform float4 _offset_1;
            uniform float4 _offset_2;
            uniform float4 _offset_3;
            uniform float _tree_size;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 node_44 = frac((o.uv0.rg*20.0)).rgg;
                float node_37_if_leA = step(node_44.r,0.3333);
                float node_37_if_leB = step(0.3333,node_44.r);
                float node_45_if_leA = step(node_44.g,0.5);
                float node_45_if_leB = step(0.5,node_44.g);
                v.vertex.xyz += cross(v.normal,(lerp((node_37_if_leA*_offset_1.rgb)+(node_37_if_leB*lerp((node_45_if_leA*_offset_2.rgb)+(node_45_if_leB*_offset_3.rgb),_offset_2.rgb,node_45_if_leA*node_45_if_leB)),_offset_1.rgb,node_37_if_leA*node_37_if_leB).rgb*_tree_size));
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_144 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_main_normal,TRANSFORM_TEX(node_144.rg, _main_normal))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.rgb*2;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * tex2D(_main_texture,TRANSFORM_TEX(node_144.rg, _main_texture)).rgb;
/// Final Color:
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _main_texture; uniform float4 _main_texture_ST;
            uniform sampler2D _main_normal; uniform float4 _main_normal_ST;
            uniform float4 _offset_1;
            uniform float4 _offset_2;
            uniform float4 _offset_3;
            uniform float _tree_size;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float3 node_44 = frac((o.uv0.rg*20.0)).rgg;
                float node_37_if_leA = step(node_44.r,0.3333);
                float node_37_if_leB = step(0.3333,node_44.r);
                float node_45_if_leA = step(node_44.g,0.5);
                float node_45_if_leB = step(0.5,node_44.g);
                v.vertex.xyz += cross(v.normal,(lerp((node_37_if_leA*_offset_1.rgb)+(node_37_if_leB*lerp((node_45_if_leA*_offset_2.rgb)+(node_45_if_leB*_offset_3.rgb),_offset_2.rgb,node_45_if_leA*node_45_if_leB)),_offset_1.rgb,node_37_if_leA*node_37_if_leB).rgb*_tree_size));
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float2 node_145 = i.uv0;
                float3 normalLocal = UnpackNormal(tex2D(_main_normal,TRANSFORM_TEX(node_145.rg, _main_normal))).rgb;
                float3 normalDirection =  normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i)*2;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * tex2D(_main_texture,TRANSFORM_TEX(node_145.rg, _main_texture)).rgb;
/// Final Color:
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _offset_1;
            uniform float4 _offset_2;
            uniform float4 _offset_3;
            uniform float _tree_size;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float2 uv0 : TEXCOORD5;
                float3 normalDir : TEXCOORD6;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                float3 node_44 = frac((o.uv0.rg*20.0)).rgg;
                float node_37_if_leA = step(node_44.r,0.3333);
                float node_37_if_leB = step(0.3333,node_44.r);
                float node_45_if_leA = step(node_44.g,0.5);
                float node_45_if_leB = step(0.5,node_44.g);
                v.vertex.xyz += cross(v.normal,(lerp((node_37_if_leA*_offset_1.rgb)+(node_37_if_leB*lerp((node_45_if_leA*_offset_2.rgb)+(node_45_if_leB*_offset_3.rgb),_offset_2.rgb,node_45_if_leA*node_45_if_leB)),_offset_1.rgb,node_37_if_leA*node_37_if_leB).rgb*_tree_size));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _offset_1;
            uniform float4 _offset_2;
            uniform float4 _offset_3;
            uniform float _tree_size;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                float3 node_44 = frac((o.uv0.rg*20.0)).rgg;
                float node_37_if_leA = step(node_44.r,0.3333);
                float node_37_if_leB = step(0.3333,node_44.r);
                float node_45_if_leA = step(node_44.g,0.5);
                float node_45_if_leB = step(0.5,node_44.g);
                v.vertex.xyz += cross(v.normal,(lerp((node_37_if_leA*_offset_1.rgb)+(node_37_if_leB*lerp((node_45_if_leA*_offset_2.rgb)+(node_45_if_leB*_offset_3.rgb),_offset_2.rgb,node_45_if_leA*node_45_if_leB)),_offset_1.rgb,node_37_if_leA*node_37_if_leB).rgb*_tree_size));
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
