﻿using UnityEngine;
using System.Collections;

public class HitBox : MonoBehaviour {

public float invulnerableTime;
Timer        t;
//Collider     trigger;

void Start() {
	
   t = new Timer( invulnerableTime );
   //trigger = collider;
}
	
void Update() {
	
}

void OnTriggerEnter( Collider col ) {

   if (!t.on && col.tag == "Enemy") {
      
      Protagonist.Hit( col.GetComponent< Enemy >().attackPower );
      t.Start();
   }
}

}
