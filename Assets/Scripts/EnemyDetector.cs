﻿using UnityEngine;
using System.Collections;

public class EnemyDetector : MonoBehaviour {

Enemy enemy;

void Awake() {
	
   enemy = transform.parent.GetComponent< Enemy >();
}
	
void Update() {
	
}

void OnTriggerEnter( Collider col ) {

   if (col.tag == "Player") {

      enemy.PlayerInRange( true );
   }
}

void OnTriggerExit( Collider col ) {

   if (col.tag == "Player") {

      enemy.PlayerInRange( false );
   }
}

}
