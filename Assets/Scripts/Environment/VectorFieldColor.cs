﻿using UnityEngine;
using System.Collections;

public class VectorFieldColor : MonoBehaviour {

public float strength;
public float speed;
public float bigSpeed;
public float scale;
public float bigScale;
public float bigPower;
float cos30 = Mathf.Cos( 30.0f * Mathf.Deg2Rad );
static VectorFieldColor instance;

void Start() {
	
   instance = this;
}

public static Vector3 WindAtPosition( Vector3 pos ) {

   return instance.GetWindAtPosition( pos );
}

Vector3 GetWindAtPosition( Vector3 pos ) {

   float dist = speed * Time.time + 100.0f;
   float bigDist = bigSpeed * Time.time + 500.0f;

   float wN  = Mathf.PerlinNoise( 0.0f + pos.x / scale, -dist + pos.z / scale );
   float wSE = Mathf.PerlinNoise( -cos30 * dist + pos.x / scale, 0.5f * dist + pos.z / scale );
   float wSW = Mathf.PerlinNoise(  cos30 * dist + pos.x / scale, 0.5f * dist + pos.z / scale );

   float bigWN  = Mathf.PerlinNoise( 0.0f + pos.x / bigScale, -bigDist + pos.z / bigScale );
   float bigWSE = Mathf.PerlinNoise( -cos30 * bigDist + pos.x / bigScale, 0.5f * bigDist + pos.z / bigScale );
   float bigWSW = Mathf.PerlinNoise(  cos30 * bigDist + pos.x / bigScale, 0.5f * bigDist + pos.z / bigScale );

   float   bigWind = Mathf.Pow( (bigWN + bigWSE + bigWSW) / 3.0f, bigPower ) * strength * bigPower;
   Vector3 wind    = new Vector3( cos30 * wSE + -cos30 * wSW, 0.0f, wN + -0.5f * wSE + -0.5f * wSW );

   return wind.normalized * bigWind;
}
	
void Update() {

}

}
