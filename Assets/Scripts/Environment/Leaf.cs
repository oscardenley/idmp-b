﻿using UnityEngine;
using System.Collections;

public class Leaf : MonoBehaviour {

static Timer t = new Timer( 0.15f );
Transform tf;

void Start() {
	
   tf = transform;
   t.Start();
   collider.enabled = false;
   rigidbody.isKinematic = true;
}
	
void Update() {
	
   if (t != null && t.stopping) {

      if (rigidbody.isKinematic && (PlayerInput.playertf.position - tf.position).sqrMagnitude < 250.0f) { rigidbody.isKinematic = false; collider.enabled = true; }
      
      if (!rigidbody.isKinematic) {
         Vector3 wind = VectorFieldColor.WindAtPosition( tf.position );
         rigidbody.AddForce( new Vector3( wind.x, wind.magnitude * 0.5f, wind.z ) * 10.0f );
         Debug.DrawLine( tf.position, tf.position + wind, Color.white, t.period );

         if ((PlayerInput.playertf.position - tf.position).sqrMagnitude > 250.0f) { rigidbody.isKinematic = true; collider.enabled = false; }
      }
      t.period = Random.Range( 0.1f, 0.2f );
   }
   if (t != null && t.dormant) {
      t.Start();
   }
}

}
