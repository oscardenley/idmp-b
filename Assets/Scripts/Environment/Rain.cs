﻿using UnityEngine;
using System.Collections;

public class Rain : MonoBehaviour {

public float          windForce;
Timer                 timer;
public Transform      windZone;
public Transform      windAnchor;
Transform             playertf;
static ParticleSystem ps;
static AudioSource    aSource;
static float          maxEmission;

void Awake() {
	
   ps              = GetComponent< ParticleSystem >();
   maxEmission     = ps.emissionRate;
   ps.emissionRate = 0.0f;
   aSource         = audio;
   aSource.volume  = 0.0f;
   playertf        = GameObject.Find( "Player" ).transform;
   timer           = new Timer( 0.1f ).Start();
}

public static float volume {
   set {
      float level     = 1.0f - value / 100.0f;
      aSource.volume  = 0.075f * level;
      ps.emissionRate = level * maxEmission;
   }
}

void Update() {

   windZone.position = windAnchor.position;
	
   if (timer.stopping) {

      Vector3 wind  = VectorFieldColor.WindAtPosition( playertf.position );
      Vector3 wind3 = new Vector3( wind.x, -0.75f, wind.z );

      Debug.DrawLine( windZone.position, windZone.position + new Vector3( wind3.x, 0.0f, wind3.z ) * 3.0f, Color.red );
      windZone.rotation = Quaternion.LookRotation( wind3.normalized );

      timer.Start();
   }
}

}
