﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class TreeMotion : MonoBehaviour {

   public int                   modelNum;
   public int                   numLeafVectors  = 3;
   public float                 leafMotionSpeed = 0.8f;
   public float                 activeRange     = 15.0f;
   public Transform          [] branches;
   public SkinnedMeshRenderer[] branchMeshes;

   bool                         inRange = false;
   Vector3                      pos;
   Material                     leavesMat;
   static Timer                 timer = new Timer( 0.15f );
   Transform                    tf;
   Quaternion                [] baseRotations;
   ZeroSpring                [] springs;
   float                     [] meshNoiseOffsets;

   void Start() {

      tf                         = transform;
      pos                        = tf.position;
      baseRotations              = new Quaternion[ branches.Length ];
      springs                    = new ZeroSpring[ branches.Length ];
      activeRange               *= activeRange;  // use square magnitude
      meshNoiseOffsets           = new float[ numLeafVectors * 6 ];
      for (int i = 0; i < meshNoiseOffsets.Length; i++) { meshNoiseOffsets[i] = Random.Range( 0, 100.0f ); }
      leavesMat                  = new Material( branchMeshes[0].material );
      leavesMat.mainTextureScale = new Vector2( transform.localScale.x, transform.localScale.z );
      leavesMat.SetFloat( "_tree_size", tf.localScale.x );
      timer.Start();

      for (int i = 0; i < branches.Length; i++) {
      
         springs      [i]          = new ZeroSpring( 0.4f, Random.Range( 2.5f, 6.5f ), Random.Range( 0.3f, 0.7f ) );
         baseRotations[i]          = branches[i].rotation;
         branchMeshes [i].material = leavesMat;
      }
      // set vertex colours derived from position, to be used in shader for leaf motion
      foreach (SkinnedMeshRenderer smr in branchMeshes) {

         Mesh mesh    = smr.sharedMesh;
         mesh.colors  = new Color[ mesh.vertexCount ];
         float minY   = mesh.vertices.Min( v => v.y );
         float yRange = mesh.vertices.Max( v => v.y ) - minY;

         for (int i = 0; i < mesh.vertexCount; i++) {

            Vector4 mc = Vector4.zero;
            for (int c = 0; c < 3; c++) { mc[c] = Mathf.Abs( mesh.vertices[i][c] * 100.0f ) % 1.0f; }
            mesh.colors[i]   = mc.normalized;
            mesh.colors[i].a = (mesh.vertices[i].y - minY) / yRange;
         }
      }
   }
	
   // TODO: pass in wind vector rather than just power - could be used for more realistic results?
   void Update() {

      if (timer != null && timer.stopping)
         { inRange = (PlayerInput.playertf.position - pos).sqrMagnitude < activeRange; }
      // allow all trees to update before restarting timer
      if (timer != null && timer.dormant) { timer.Start(); }
      if (!inRange) { return; }

      Vector3 wind      = VectorFieldColor.WindAtPosition( pos );
      float   windPower = wind.magnitude;
      float   t         = Time.time * leafMotionSpeed;

      for (int i = 0; i < branches.Length; i++) {
         
         Vector3 swayDirection = springs[i].Update( wind );
         Vector3 axis          = Vector3.Cross( Vector3.up, swayDirection );
         branches[i].rotation  = baseRotations[i];
         branches[i].Rotate( axis, swayDirection.magnitude * 25.0f, Space.World );
      }

      for (int i = 0; i < numLeafVectors; i++) {
         Vector4 rand = Vector4.zero;
         for (int j = 0; j < 3; j++) {
            rand[j] = Mathf.PerlinNoise( t + i + meshNoiseOffsets[ 6*i + 2*j     ],
                                         t + i + meshNoiseOffsets[ 6*i + 2*j + 1 ] );
         }
         leavesMat.SetVector( "_offset_" + (i+1), rand * 0.2f * windPower );
      }
   }
}
