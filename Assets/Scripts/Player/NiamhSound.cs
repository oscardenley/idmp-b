﻿using UnityEngine;
using System.Collections;

public class NiamhSound : MonoBehaviour {

   public AudioClip[] footstepSounds;
   public AudioClip[] attackSounds;

   void Start() {

   }

   void Update() {

   }

   void PlayFootstepSound() {

      audio.PlayOneShot( footstepSounds[ Random.Range( 0, footstepSounds.Length ) ] );
   }

   void PlayAttackSound() {

      audio.PlayOneShot( attackSounds[ Random.Range( 0, attackSounds.Length ) ] );
   }

}
