﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CamMotion : MonoBehaviour {

   Transform        tf;
   public Transform player;
   public float     damping;
   public float     rotDamping;
   public float     rockReturnSpeed;
   public float     tilt;
   public float     maxRockDistance, targetDamping;
   float            musicOffset;
   Vector3          rock, relPos, relTarget, upDir;
   SortedDictionary< float, Vector3 > lastVels = new SortedDictionary< float, Vector3 >();
   public float     queueLength = 0.33f;  // no. of frames as fraction of a second

   void Awake() {

      AudioListener.volume = 0.0f;
      musicOffset          = Random.Range( 0.0f, 100.0f );
   }

   void Start() {

      //Application.targetFrameRate = 30;
      tf     = transform;
      rock   = player.position;
      relPos = tf.position - player.position;
      tf.LookAt( player.position );
      upDir  = tf.up * tilt + Vector3.up * (1 - tilt);
      AudioListener.volume = 1.0f;
   }

   void LateUpdate() {

      // calculate target aim and target position
      rock                    = Vector3.Lerp( rock, player.position, rockReturnSpeed * Time.deltaTime );
      rock                    = player.position + Vector3.ClampMagnitude( rock - player.position, maxRockDistance );
      relTarget               = Vector3.Lerp( relTarget, player.position - rock, targetDamping * Time.deltaTime );
      Vector3 target          = player.position + relTarget + Vector3.up * 0.5f;
      Vector3 targetPos       = target + relPos;
      targetPos.z             = Mathf.Clamp( targetPos.z, LevelGenerator.lowerEdge, Mathf.Infinity );
      // calculate and enqueue target velocity
      Vector3 dampedTargetPos = Vector3.Lerp( tf.position, targetPos, Mathf.Clamp01( damping * Time.deltaTime ) );
      lastVels.Add( Time.time, (dampedTargetPos - tf.position) / Time.deltaTime );
      // dequeue velocities beyond (rough) time specified by queueLength
      while (lastVels.First().Key < Time.time - queueLength) { lastVels.Remove( lastVels.First().Key ); }
      // use average of recent target velocities as new velocity
      Vector3 velSum          = Vector3.zero;
      foreach (Vector3 vel in lastVels.Values) { velSum += vel; }
      tf.position            += (velSum / lastVels.Count) * Time.deltaTime;
      // add some lerping to counteract camera drift due to velocity deltatimes being different
      tf.position             = Vector3.Lerp( tf.position, dampedTargetPos, Time.deltaTime * 0.5f );
      Quaternion targRot      = Quaternion.LookRotation( target - tf.position, upDir );
      tf.rotation             = Quaternion.Slerp( tf.rotation, targRot, rotDamping * Time.deltaTime );

      // music
      audio.volume = Mathf.Clamp01( Mathf.PerlinNoise( musicOffset + Time.time / 30.0f, 0.0f ) - 0.5f * 0.5f);
   }
}
