﻿using UnityEngine;
using System.Collections;

public class FairyChantCircle : MonoBehaviour {

//Fairy fairy;
public float rotSpeed;
Transform tf;

void Awake() {

   tf    = transform;
   //fairy = GameObject.Find("Fairy").GetComponent< Fairy >();
}

void Start() {
	
}
	
void Update() {
	
   tf.Rotate( Vector3.up, rotSpeed * Time.deltaTime );
}

void OnTriggerEnter( Collider col ) {

   if (col.tag != "Enemy") { return; }
   Fairy.enemies.Add( col.GetComponent< Enemy >() );
}

void OnTriggerExit( Collider col ) {

   if (col.tag != "Enemy") { return; }
   Enemy enemy = col.GetComponent< Enemy >();
   Fairy.enemies.Remove( enemy );
   enemy.LeaveChant();
}

}
