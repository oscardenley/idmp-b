﻿using UnityEngine;
using System.Collections;

public class FairyTargetOrbit : MonoBehaviour {

public float orbitSpeed;
public Vector3 relPos;
public bool orbiting = true;
float nonOrbitTime = 0.0f;
Transform tf;
Transform ptf;

void Awake() {
	
   tf = transform;
   ptf = GameObject.Find("Player").transform;
   relPos = tf.position - ptf.position;
}
	
void Update() {
	
   if (!orbiting) { nonOrbitTime += Time.deltaTime; }
   float angle = ((Time.time - nonOrbitTime) * 2.0f * Mathf.PI * orbitSpeed) % (Mathf.PI * 2.0f);

   tf.position = ptf.position + new Vector3( Mathf.Cos( angle ) * relPos.x,
                                             relPos.y,
                                             Mathf.Sin( angle ) * relPos.x );
}

}
