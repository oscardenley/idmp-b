﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum FairyState { IDLE, MOVING, FOLLOWING, CHANTING }

public class Fairy : MonoBehaviour {

public Transform     orbitTarget;
Transform            highlightGlow;
bool                 highlighted;
public float         idleSpeed;  // speed of motion when orbiting player
public float         movementSpeed;  // speed of motion towards chant centre
CapsuleCollider      chantCircle;
Transform            tf;
Vector3              chantCentre;
Vector3              cursorPos;
public FairyState    state;
public static List< Enemy > enemies = new List< Enemy >();
public Color         idleColor;
public Color         chantColor;
ParticleSystem       ps;
float                hoverHeight;
float                chantRadius;
Collider[]           glowTiles;

void SetColor( Color color ) {

   renderer.material.color = color;
   ps.startColor           = color;
   light.color             = color;
}

public void Highlight( bool highlight ) {

   highlighted = highlight;
   orbitTarget.GetComponent< FairyTargetOrbit >().orbiting = !highlight;
   highlightGlow.renderer.enabled = highlight;
   if (highlighted) { SetHighlightDir(); }
}

public void FollowPoint( Vector3 cursorPos ) {

   state          = FairyState.FOLLOWING;
   this.cursorPos = cursorPos;
}

public void ChantCommand( Vector3 centre, float radius ) {

   StopChanting();
   state       = FairyState.MOVING;
   chantRadius = radius;
   chantCentre = centre;
   chantCircle.transform.localScale = new Vector3( 2.0f * radius,
                                                   chantCircle.transform.localScale.y,
                                                   2.0f * radius );
   SetColor( idleColor );
}

public void StopChanting() {

   chantCircle.GetComponent<ParticleSystem>().Stop();
   if (glowTiles != null) { foreach (Collider c in glowTiles) { c.GetComponent< Tile >().Glow( false ); } }
   chantCircle.radius = 0.0f;
}

public void StartChanting() {

   chantCircle.radius = 0.5f;
   chantCircle.transform.position = tf.position - Vector3.up * hoverHeight;
   chantCircle.GetComponent<ParticleSystem>().Play();
   glowTiles = Physics.OverlapSphere( chantCentre, chantRadius, 1 << LayerMask.NameToLayer( "Ground" ) );

   foreach (Collider c in glowTiles) {

      Vector3 dir = (c.transform.position - chantCentre);
      dir -= Vector3.up * dir.y;
      c.GetComponent< Tile >().Glow( true, 1.1f - dir.magnitude / chantRadius );
   }
   SetColor( chantColor );
}

public void IdleCommand() {

   state = FairyState.IDLE;
   StopChanting();
   SetColor( idleColor );
}

void Awake() {

   ps            = GetComponent< ParticleSystem >();
   tf            = transform;
   chantCircle   = GameObject.Find("ChantCircle").GetComponent< CapsuleCollider >();
   highlightGlow = tf.Find("Highlight");
}

void Start() {

   hoverHeight = orbitTarget.GetComponent< FairyTargetOrbit >().relPos.y;
   SetColor( idleColor );
}

void SetHighlightDir() {
   
   Vector3 highlightDir = highlightGlow.position - Camera.main.transform.position;
   highlightGlow.rotation = Quaternion.LookRotation( highlightDir );
}

bool Move( Vector3 destination, Vector3 oldPos, float speed ) {
   
   if ((tf.position - destination).sqrMagnitude < 0.01f) { return true; }
   tf.position = Vector3.MoveTowards( tf.position, destination,
                                       speed * Time.deltaTime );
   tf.rotation = Quaternion.LookRotation( tf.position - oldPos );
   return false;
}
	
void Update() {

   Vector3 oldPos = tf.position;
	
   switch (state) {

      case FairyState.IDLE: {

         Move( orbitTarget.position, oldPos, idleSpeed );
         break;
      }
      case FairyState.FOLLOWING: {

         Move( cursorPos + Vector3.up * hoverHeight, oldPos, movementSpeed );
         break;
      }
      case FairyState.MOVING: {

         if (Move( chantCentre + Vector3.up * hoverHeight, oldPos, movementSpeed )) {

            StartChanting();
            state = FairyState.CHANTING;
         }
         break;
      }
      case FairyState.CHANTING: {

         foreach (Enemy enemy in enemies) { enemy.ApplyChant( (5.0f / chantRadius) ); }
         break;
      }
   }
   if (highlighted) { SetHighlightDir(); }
}

}
