﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum InputState { DEFAULT, MOVEMENT, DEFORM_DRAG, FAIRY_HIGHLIGHT, FAIRY_PLACEMENT, CHANT_DRAG }

public class PlayerInput : MonoBehaviour {

          Protagonist          player;
          ThirdPersonCharacter tpc;
   static Transform            tf;
   [HideInInspector]
   public float                h = 0.0f, v = 0.0f;
          Vector3              destination;
          Timer                logUpdate = new Timer( 0.5f );
          Tile                 cursorTile;
          List< Tile >         deformTiles;
          float                dragBase, heightBase, lastDragHeight;
          DeformationPower     dp, raiseFeedback;
   public Transform            terrainSoundTF;
   public TerrainSound         terrainSound;
          Tile                 currSoundTile;
   public InputState           state = InputState.DEFAULT;
   public Transform            marker;
          Vector3              chantCentre;
          Fairy                fairy;
   public Texture2D            fairyCursor, deformCursor, walkCursor;
   public float                cursorSize;  // as fraction of screen height
          Transform            chantCircle;
          Projector            chantIndicator;
          Cursor               cursor;
          Dictionary< InputState, Texture2D > cursorImg;
          GUIStyle             cursorStyle = new GUIStyle();
          bool                 left, right, moving;

   public static Transform playertf {
      get {
         if (!tf) { tf = GameObject.Find( "Player" ).transform; }
         return tf;
      }
   }

   void Awake() {

      player                    = GetComponent< Protagonist >();
      tf                        = transform;
      tpc                       = GetComponent< ThirdPersonCharacter >();
      DeformationPower[] powers = GetComponents< DeformationPower >();
      dp                        = powers.Single( power => power.powerName == "Wall"          );
      raiseFeedback             = powers.Single( power => power.powerName == "RaiseFeedback" );
      fairy                     = GameObject.Find("Fairy").GetComponent< Fairy >();
      chantIndicator            = GameObject.Find("Indicator").GetComponent< Projector >();
      destination               = playertf.position;

      cursorImg = new Dictionary< InputState, Texture2D >() {

         { InputState.DEFAULT,         walkCursor   },
         { InputState.DEFORM_DRAG,     deformCursor },
         { InputState.MOVEMENT,        walkCursor   },
         { InputState.FAIRY_PLACEMENT, fairyCursor  },
         { InputState.FAIRY_HIGHLIGHT, fairyCursor  },
         { InputState.CHANT_DRAG,      fairyCursor  }
      };
   }

   void Start() {

      //Screen.showCursor = false;
   }

   void SetState( InputState s ) {
   
      state = s;
   }

   void StartDeformDrag() {
   
      SetState( InputState.DEFORM_DRAG );
      player.SetState( PlayerState.DEFORM_START );
      destination = tf.position;  // halt in place when deforming
      deformTiles = new List< Tile >();
      deformTiles.Add( cursorTile );
      cursorTile.Deform( raiseFeedback.StartDeformation() );
   }

   void StartFairyHighlight() {

      SetState( InputState.FAIRY_HIGHLIGHT );
      cursorTile.Highlight( InputState.FAIRY_HIGHLIGHT );
      fairy.Highlight( true );
   }

   void StartChantDrag( Vector3 point ) {
   
      SetState( InputState.CHANT_DRAG );
      chantIndicator.enabled = true;
      chantIndicator.transform.position = point + Vector3.up * 10.0f;
      chantCentre = point;
   }

   void EndFairyHighlight() {

      fairy.Highlight( false );
      //player.SetState( PlayerState.DEFORM_END );
      SetState( InputState.DEFAULT );
      cursorTile.Highlight( state );
   }

   void EndDeformDrag() {

      foreach (Tile t in deformTiles) {

         t.Deform( dp, dp.maxSustainTime / deformTiles.Count );
         t.Deform( raiseFeedback.StartDeformation( true ) );
      }
      terrainSoundTF.position = cursorTile.top;
      //Transform sound = Instantiate( deformationSound, cursorTile.tf.position, Quaternion.identity ) as Transform;
      //sound.parent    = cursorTile.transform;
      if (currSoundTile) { currSoundTile.terrainSound = null; }
      currSoundTile = cursorTile;
      currSoundTile.terrainSound = terrainSound;
      terrainSound.StartSound( TerrainSFX.rumble );
      player.PlayStateAnimation( PlayerState.DEFORM_END );
      player.SetState( PlayerState.IDLE );
      SetState( InputState.DEFAULT );
   }

   void EndChantDrag( Vector3 point ) {

      float radius = (new Vector3( point.x, 0.0f, point.z ) - new Vector3( chantCentre.x, 0.0f, chantCentre.z )).magnitude;
      fairy.ChantCommand( chantCentre, radius );
      chantIndicator.enabled = false;
      chantIndicator.orthographicSize = 0.0f;
      SetState( InputState.DEFAULT );
   }

   void MovementInput( Vector3 point ) {
   
      tpc.lookTarget  = null;
      Vector3 moveDir = Vector3.zero;
      moveDir         = point - tf.position;
      moveDir        -= Vector3.up * moveDir.y;
      moveDir         = moveDir.normalized;
      h               = moveDir.x;
      v               = moveDir.z;
      if (!logUpdate.on) { logUpdate.Start(); }
   }

   void Update() {
   
      //if (Screen.showCursor) { Screen.showCursor = false; }
      bool newCursorTile = false;
      bool leftDown = false, leftUp = false, rightDown = false, rightUp = false;

      if ( Input.GetMouseButton(0) && !left ) { left  = true;  leftDown  = true; }
      if (!Input.GetMouseButton(0) &&  left ) { left  = false; leftUp    = true; }
      if ( Input.GetMouseButton(1) && !right) { right = true;  rightDown = true; }
      if (!Input.GetMouseButton(1) &&  right) { right = false; rightUp   = true; }
   
      Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
      RaycastHit hit;

      h = 0.0f;
      v = 0.0f;

      if (Input.GetKeyDown( "escape" )) { Application.Quit(); }
      if (Input.GetKeyDown( "r"      )) { Application.LoadLevel( Application.loadedLevel ); }

      int  mask   = state == InputState.FAIRY_PLACEMENT ? ~(1 << 11 | 1 << 2 | 1 << 13) : ~(1 << 2 | 1 << 13);
      bool rayHit = Physics.Raycast( ray, out hit, Mathf.Infinity, mask );

      if (rayHit) {

         if (hit.transform.gameObject.layer == 10) {  // hit a tile

            Tile hitTile = hit.transform.GetComponent< Tile >();

            if (hitTile != cursorTile) {

               cursorTile    = hitTile;
               newCursorTile = true;
            }
            cursorTile.Highlight( state );
         }
      }
      else { return; }

      switch (state) {

         case InputState.DEFAULT: {
         
            if (hit.transform.tag == "Fairy") { StartFairyHighlight(); }
            else if (leftDown && hit.transform.gameObject.layer == 10)
               { SetState( InputState.MOVEMENT ); }
            else if (rightDown && player.state != PlayerState.FALL) { StartDeformDrag(); }
            break;
         }
         case InputState.MOVEMENT: {

            destination = hit.point;
            moving      = true;
            if (leftUp) { SetState( InputState.DEFAULT ); }
            break;
         }
         case InputState.FAIRY_PLACEMENT: {
         
            fairy.FollowPoint( hit.point );
            if (leftDown) { StartChantDrag( hit.point ); }

            else if (rightDown) {

               fairy.IdleCommand();
               SetState( InputState.DEFAULT );
            }
            break;
         }
         case InputState.FAIRY_HIGHLIGHT: {
         
            if (hit.transform.tag != "Fairy") { EndFairyHighlight(); }

            if (leftDown) {

               if (fairy.state != FairyState.CHANTING) {

                  EndFairyHighlight();
                  SetState( InputState.FAIRY_PLACEMENT );
               }
               else {
               
                  fairy.IdleCommand();
                  SetState( InputState.DEFAULT );
               }
            }
            break;
         }
         case InputState.DEFORM_DRAG: {

            tpc.lookTarget = cursorTile.tf;
            if (rightUp) { EndDeformDrag(); }

            if (newCursorTile && !deformTiles.Contains( cursorTile )) {
               
               deformTiles.Add( cursorTile );
               cursorTile.Deform( raiseFeedback.StartDeformation() );
            }
            break;
         }
         case InputState.CHANT_DRAG: {

            float radius = (new Vector2( chantCentre.x, chantCentre.z )
                          - new Vector2( hit.point.x,   hit.point.z   )).magnitude;
            chantIndicator.orthographicSize = radius * 1.1f;
            if (leftUp) { EndChantDrag( hit.point ); }
            break;
         }
      }
      if (state != InputState.DEFORM_DRAG && moving) {

         Vector3 movement = playertf.position - destination;
         movement.y       = 0.0f;

         if (movement.sqrMagnitude > 0.05f) {

            MovementInput( destination );
            player.SetState( PlayerState.RUN );
         }
         else {

            moving = false;
            player.SetState( PlayerState.IDLE );
         }
      }
      if (Input.GetKeyDown("t")) {
         print("Number of timers: " + Timer.numTimers);
      }
   }

   void OnGUI() {

      Rect cursorRect = new Rect( Input.mousePosition.x, -Input.mousePosition.y + Screen.height,
                                  Screen.height * cursorSize, Screen.height * cursorSize );
      GUI.Box( cursorRect, cursorImg[ state ], cursorStyle );
   }

}
