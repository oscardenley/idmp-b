﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum PlayerState { WALK, IDLE, FLINCH, RUN, DEFORM_START, DEFORM_END, FALL }

public class Protagonist : MonoBehaviour {

   public static Tile  currTile;
   public static float health = 100.0f;
   private const int   mask   = 1 << 10;
   public static Transform           tf;
   static Protagonist  instance;
   Queue< float >      frameTimes = new Queue< float >();
   public Collider     hitBox;
   public PlayerState  state;
   public Animation    niamh;
   public Animation    cape;
   Timer               animTimer = new Timer( 0.6f );
   public Animation    soundAnimation;
   public GUIStyle     remainingStyle;
   public static int   numEnemies = 0;
   public static Timer numRemainingDisplay = new Timer( 3.0f );
   public GUIStyle     fpsStyle;

   struct AnimData {

      public string body, cape, sound;
      public float  fadeTime;

      public AnimData( string b, string c, string s, float f ) {

         body     = b;
         cape     = c;
         sound    = s;
         fadeTime = f;
      }
   } 

   Dictionary< PlayerState, AnimData > anims = new Dictionary< PlayerState, AnimData > {
   
      { PlayerState.IDLE,         new AnimData( "Niamh_Idle",             "Cape_idle",             null,      0.4f  ) },
      { PlayerState.WALK,         new AnimData( "Niamh_Walking",          "Cape_walking",          null,      0.2f  ) },
      { PlayerState.FLINCH,       new AnimData( "Niamh_Flinch",           "Cape_flinch",           null,      0.05f ) },
      { PlayerState.RUN,          new AnimData( "Niamh_Running",          "Cape_running",          "Running", 0.2f  ) },
      { PlayerState.DEFORM_START, new AnimData( "Niamh_EarthManip_part1", "Cape_earthManip_part1", null,      0.1f  ) },
      { PlayerState.DEFORM_END,   new AnimData( "Niamh_EarthManip_part2", "Cape_earthManip_part2", null,      0.0f  ) },
      { PlayerState.FALL,         new AnimData( "Niamh_Fall",             "Cape_fall",             null,      0.3f  ) },
   };

   public static Vector3 WindAtPlayer {
      get {
         return VectorFieldColor.WindAtPosition( tf.position );
      }
   }

   public void SetState( PlayerState newState, bool landing=false ) {

      if (!(landing ^ state == PlayerState.FALL)) {  // only landing can cause falling state to exit

         if (!animTimer.on && state != newState) { PlayStateAnimation( newState ); }
         state = newState;
      }
   }

   public void PlayStateAnimation( PlayerState newState ) {

      if (!niamh) { return; }

      niamh.CrossFade( anims[ newState ].body, anims[ newState ].fadeTime );
      cape .CrossFade( anims[ newState ].cape, anims[ newState ].fadeTime );

      if (anims[ newState ].sound != null) { soundAnimation.Play( anims[ newState ].sound ); }
      else { soundAnimation.Stop(); }

      if (newState == PlayerState.DEFORM_END || newState == PlayerState.FLINCH) { animTimer.Start(); }
   }

   public static void Hit( float damage ) {
   
      health -= damage;
      instance.PlayStateAnimation( PlayerState.FLINCH );
      Rain.volume = health;
   }

   void Awake() {
   
      instance = this;
      tf       = transform;
      health   = 100.0f;
   }

   /*public static void StartDisplayTimer() {

      numEnemies = GameObject.FindObjectsOfType< Gruagach >().Length;
      numRemainingDisplay.Start();
   }*/

   void Start() {
   
      //StartDisplayTimer();
   }

   void DepressTile() {

      Ray ray = new Ray( tf.position + Vector3.up, Vector3.down );
      RaycastHit hit;

      if (Physics.Raycast( ray, out hit, 20.0f, mask )) {

         if (currTile == null || hit.transform != currTile.tf) {

            currTile = hit.transform.GetComponent< Tile >();
            currTile.Depress();
         }
      }
   }

   void Update() {

      DepressTile();

      if (currTile && tf.position.y < currTile.lowestTopPoint) {
         tf.position = new Vector3( tf.position.x, currTile.highestPoint, tf.position.z );
      }
      if (health <= 0.0f || Input.GetKeyDown("r")) {

         numEnemies = 0;
         Application.LoadLevel( Application.loadedLevel );
      }
      frameTimes.Enqueue( Time.deltaTime );
      if (frameTimes.Count > 5) { frameTimes.Dequeue(); }
      if (animTimer.stopping) { PlayStateAnimation( state ); }
   }

   void OnGUI() {

      Rect fpsRect = new Rect( Screen.width - 75, 25, 50, 50 );
      GUI.Box( fpsRect, (1.0f / frameTimes.Average()).ToString("0.00"), fpsStyle );

      Rect healthRect = new Rect( Screen.width - 75, 75, 50, 50 );
      GUI.Box( healthRect, health.ToString(), fpsStyle );

      Rect numRemainingRect = new Rect( 30, 100, 200, 100 );
      Rect restartRect      = new Rect( 30, 20, 200, 100 );

      if (GUI.Button( restartRect, "Restart", fpsStyle )) {

         numEnemies = 0;
         Application.LoadLevel( Application.loadedLevel );
      }

      GUI.Box( numRemainingRect, "Enemies remaining: " + numEnemies, fpsStyle );
   }

}
