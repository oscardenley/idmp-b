﻿using UnityEngine;
using System.Collections;

public class TileDetection : MonoBehaviour {

Timer timer;

void Start() {

	timer = new Timer( 2.0f );
   timer.Start();
}

void Update() {
	
   if (timer.stopping) {

      Collider[] cols = Physics.OverlapSphere( transform.position, GetComponent< SphereCollider >().radius );
      foreach (Collider col in cols) { col.enabled = true; }
   }
   timer.Start();
}

void OnTriggerEnter( Collider col ) {

   if (col.GetComponent< Tile >()) { col.enabled = true; }
}

void OnTriggerExit( Collider col ) {

   if (col.GetComponent< Tile >()) { col.enabled = false; }
}

}
