﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using Pathfinding;

public enum EnemyState { IDLE, PURSUE, RETURN, ATTACK, FLINCH }

[RequireComponent( typeof( ParticleSystem ) )]
public class Enemy : MonoBehaviour {

   public    float          detectionRange;
   public    float          speed;
   public    float          hp;
   float                    maxHP;
   public    float          attackPower;
   protected Vector3        homePosition;
   protected Transform      tf;
   protected ParticleSystem hpEffect;
   float                    fullHPemission;
   protected ParticleSystem cleanseEffect;
   protected float          maxCleanseEmission;
   protected Transform      playerTf;
   protected SphereCollider detection;
   protected Seeker         seeker;
   protected bool           playerInRange = false;
   protected Path           path;
   protected float          waypointRadius = 1.0f;
   protected float          distToRecalculate = 1.0f;
   int                      currentWaypoint;
   Vector3                  destination;
   EnemyState               state = EnemyState.IDLE;
   public    Animation      model;
   public    Animation      eyeball;
   Timer                    attackTimer;
   Timer                    animTimer  = new Timer( 0.6f );
   Timer                    stateHold  = new Timer( 1.0f );  // don't interrupt partway through attack
   Timer                    blinkTimer = new Timer( 3.0f );
   const string             blinkAnim  = "Gruagach_blink";
   public NiamhSound        soundController;
   public Animation         soundAnimation;

   Dictionary< EnemyState, string > anims = new Dictionary< EnemyState, string > {
   
      { EnemyState.ATTACK, "Gruagach_attack" },
      { EnemyState.IDLE,   "Gruagach_idle"   },
      { EnemyState.PURSUE, "Gruagach_walk"   },
      { EnemyState.RETURN, "Gruagach_walk"   },
      { EnemyState.FLINCH, "Gruagach_flinch" }
   };

   Dictionary< EnemyState, string > soundAnims = new Dictionary< EnemyState, string > {
   
      { EnemyState.ATTACK, "EnemyAttackSound"  },
      { EnemyState.IDLE,   null                },
      { EnemyState.PURSUE, "EnemyRunningSound" },
      { EnemyState.RETURN, null                },
      { EnemyState.FLINCH, null                }
   }; 

   Dictionary< EnemyState, float > fadeTimes = new Dictionary< EnemyState, float > {
   
      { EnemyState.ATTACK, 0.1f  },
      { EnemyState.IDLE,   0.5f  },
      { EnemyState.PURSUE, 0.2f  },
      { EnemyState.RETURN, 0.2f  },
      { EnemyState.FLINCH, 0.05f }
   };

   public void LeaveChant() {

      cleanseEffect.emissionRate = 0.0f;
   }
   
   public void PlayStateAnimation( EnemyState newState ) {

      model.CrossFade( anims[ newState ], fadeTimes[ newState ] );
      if (newState == EnemyState.FLINCH ) { animTimer.Start(); }
      if (soundAnims[ newState ] != null) { soundAnimation.Play( soundAnims[ newState ] ); }
      else { soundAnimation.Stop(); }
   }

   public void SetState( EnemyState newState ) {

      if (!animTimer.on && state != newState) { PlayStateAnimation( newState ); }
      state = newState;
   }

   protected virtual void Awake() {
	
      tf                         = transform;
      detection                  = (SphereCollider)tf.Find("Detector").collider;
      detection.radius           = detectionRange;
      playerTf                   = GameObject.Find("Player").transform;
      seeker                     = GetComponent< Seeker >();
      homePosition               = tf.position;
      hpEffect                   = GetComponent< ParticleSystem >();
      fullHPemission             = hpEffect.emissionRate;
      maxHP                      = hp;
      cleanseEffect              = tf.Find("EnemyHealingParticles").GetComponent< ParticleSystem >();
      maxCleanseEmission         = cleanseEffect.emissionRate;
      cleanseEffect.emissionRate = 0.0f;
      waypointRadius            *= waypointRadius;  // only deal in square magnitudes
      distToRecalculate         *= distToRecalculate;
      Protagonist.numEnemies++;
   }

   public virtual void NodeUpdate( Bounds bounds ) {

      if (path == null) { return; }
      if (path.vectorPath.Any( bounds.Contains )) { seeker.StartPath( tf.position, destination, OnPathComplete ); }
   }

   public void OnPathComplete( Path p ) {

      path            = p;
      currentWaypoint = 0;
   }

   protected virtual void Start() {

      blinkTimer.Start();
   }

   protected void PlotPathToPlayer() {
   
      destination = playerTf.position;
      seeker.StartPath( tf.position, destination, OnPathComplete );
   }

   protected virtual void Update() {

      hpEffect.emissionRate = fullHPemission * (hp / maxHP);

      if (animTimer.stopping) { PlayStateAnimation( state ); }

      if (!blinkTimer.on) {

         eyeball.Rewind();
         eyeball.Play( blinkAnim );
         blinkTimer.period = Random.Range( 5.0f, 9.0f );
         blinkTimer.Start();
      }

      if (hp <= 0.0f) {

         //Protagonist.StartDisplayTimer();
         Protagonist.numEnemies--;
         Fairy.enemies.Remove( this );
         Destroy( gameObject );
      }
   
      if ((state == EnemyState.PURSUE || (state == EnemyState.ATTACK && !stateHold.on))
         && (playerTf.position - destination).sqrMagnitude > distToRecalculate) {
      
         PlotPathToPlayer();
         SetState( EnemyState.PURSUE );
      }

      if (path == null) { return; }

      if (currentWaypoint >= path.vectorPath.Count) {

         if (state == EnemyState.RETURN) { SetState( EnemyState.IDLE   ); }
         if (state == EnemyState.PURSUE) {

            SetState( EnemyState.ATTACK );
            stateHold.Start();
         }
         path = null;
         return;
      }

      rigidbody.AddForce( (path.vectorPath[ currentWaypoint ] - tf.position) * speed );
      tf.rotation = Quaternion.LookRotation( new Vector3( rigidbody.velocity.x, 0.0f, rigidbody.velocity.z ) );

      if ((path.vectorPath[ currentWaypoint ] - tf.position).sqrMagnitude < waypointRadius) {
         currentWaypoint++;
      }
   }

   public void ApplyChant( float power ) {

      cleanseEffect.emissionRate = 1.0f * power * maxCleanseEmission;
      hp -= power * Time.deltaTime;
   }

   public void PlayerInRange( bool inRange ) {

      playerInRange = inRange;

      if (inRange) {

         SetState( EnemyState.PURSUE );
         PlotPathToPlayer();
      }
      else {

         SetState( EnemyState.RETURN );
         destination = homePosition;
         seeker.StartPath( tf.position, destination, OnPathComplete );
      }
   }

   public void FollowPath() {


   }

}
