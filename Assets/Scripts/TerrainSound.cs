﻿using UnityEngine;
using System.Collections;

public enum TerrainSFX { rumble, crumble }

public class TerrainSound : MonoBehaviour {

   public AudioClip rumbleSound, crumbleSound;
   public float rumbleVolume;
   public TerrainSFX currSound;

   public float soundVol {
      set {
         audio.volume = currSound == TerrainSFX.rumble ? rumbleVolume * value : value;
      }
   }

   public void StartSound( TerrainSFX newSound ) {

      audio.Stop();

      switch (newSound) {

         case TerrainSFX.rumble: { audio.PlayOneShot( rumbleSound ); break; }
         case TerrainSFX.crumble: { audio.PlayOneShot( crumbleSound ); break; }
      }
   }

   public void StopSound() {

      audio.Stop();
   }

   void Start() {

   }

   void Update() {

   }

}
