﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Gem {

   public GemType gemType;
   public float   amount = 1.0f;
}

public enum GemType { violet, turquoise }