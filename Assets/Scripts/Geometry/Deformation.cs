﻿using System;
using UnityEngine;

[System.Serializable]
public class Deformation {

   public AnimationCurve curve;
   public float          scale;
   public Timer          t;
   bool                  reverse;
   bool                  inverse;

   public Deformation( AnimationCurve c, float s, float period, bool i=false, bool r=false ) {

      curve   = c;
      scale   = s;
      reverse = r;
      inverse = i;
      t       = new Timer( period );
      t.Start();
   }

   public float delta {
      get {
         float evalPoint     = reverse ? 1.0f - t.elapsedNormalized : t.elapsedNormalized;
         float prevEvalPoint = reverse ? evalPoint + Time.deltaTime / t.period
                                       : evalPoint - Time.deltaTime / t.period;
         float reverseOffset = reverse ? -curve.Evaluate( 1.0f ) : 0.0f;
         float prev          = curve.Evaluate( prevEvalPoint ) + reverseOffset;
         float baseDelta     = scale * ((curve.Evaluate( evalPoint ) - prev) + reverseOffset);
         return inverse ? -baseDelta : baseDelta;
      }
   }
}