﻿using System;
using UnityEngine;
using System.Collections.Generic;

public struct Triangle2 {

   public Vector2 a, b, c;

   public Triangle2( Vector2 a, Vector2 b, Vector2 c ) {

      this.a = a;
      this.b = b;
      this.c = c;
   }

   public Triangle2( Vector2[] v ) {

      if (v.Length < 2) { a = Vector2.zero;
                          b = Vector2.zero;
                          c = Vector2.zero;
                          return; }
      a = v[0];
      b = v[1];
      c = v[2];
   }

   public Triangle2( List< Vector2 > v ) : this( v.ToArray() ) { }

   public float area {
      get {
         float num = a.x * (b.y - c.y)
                   + b.x * (c.y - a.y)
                   + c.x * (a.y - b.y);

         return Mathf.Abs( num / 2.0f );
      }
   }
   
   public Vector2[] vertices { get { return new Vector2[] { a, b, c };         } }
   public Vector2   centroid { get { return LevelGenerator.AvgLoc( vertices ); } }
}