﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class TextureGenerator : MonoBehaviour {

public bool useNewShader;
public int slopeDivs;
public int altDivs;
public static Material[,] mat;
static TextureGenerator instance;
public Material prototypeMat;
public List< Texture2D > texs  = new List< Texture2D >();
public List< Texture2D > norms = new List< Texture2D >();
public int textureSize = 64;
public int numLayers;
public int useStyle;
public float overlapAmount = 0.1f;
float[,] noise;
float[][,] perlinLayers;
int overlap;
int max;

[System.Serializable]
public class Style {

   public Color lowFlatCol; // colour at sea level on a flat surface
   public Color lowSlopeCol; // colour at sea level on a fully sloped surface
   public Color highFlatCol; // colour at max altitude on a flat surface
   public Color highSlopeCol; // colour at max altitude on a fully sloped surface
   public Color vertCol; // colour on cliff faces
   public float grainRangeMax; // Luminance variation in each texture channel at max brightness
   public float grainRangeMin; // Luminance variation at min brightness
   public float perlin; // amount of perlin noise overlayed as a multiple of grain range
   public float multiply; // multiply all generated colours by this amount to avoid blowout
   public float balance; // how much stronger each perlin level is compared to previous
   public float bumpBalance; // as above but for the bump map
   public float bumpStrength; // strength of bump mapping
   public float bumpNoise; // how much point noise to include in bumpmap
   public bool  reuseOffsets; // use same perlin layer offsets for each alt/slope division
}

[SerializeField]
Style[] styles;

int[]  altDistribution;
int[]  slopeDistribution;
int[,] numInstances;

public static float[] altIntervals;
public static float[] slopeIntervals;

int loop( int value ) {

   return value <  0   ? max + value
        : value >= max ? value - max
        : value;
}

static float SinRamp( float value ) {

   return 0.5f * Mathf.Sin( (value - 0.5f) * Mathf.PI ) + 0.5f;
}

public static TextureGenerator GetInstance() {

   return instance;
}

void Awake() {

   instance          = this;
   altDistribution   = new int[ altDivs ];
   slopeDistribution = new int[ slopeDivs ];
   numInstances      = new int[ altDivs, slopeDivs ];
   altIntervals      = new float[ altDivs - 1 ];
   slopeIntervals    = new float[ slopeDivs - 1 ];

   mat = new Material[ altDivs, slopeDivs ];

   for (int a = 0; a < altDivs; a++) for (int s = 0; s < slopeDivs; s++) {

      mat[ a, s ] = new Material( prototypeMat ) { mainTexture = Resources.Load< Texture2D >( "texture-" + a + "-" + s ) };
      mat[ a, s ].SetTexture( "_BumpMap", Resources.Load< Texture2D >( "normal-" + a + "-" + s ) );
   }
}

Object[] GenerateBase( Color tileBase ) {  // generate texture, normal and material for base

   Style st = styles[ useStyle ];

   Texture2D tex   = new Texture2D( textureSize, textureSize );
   Texture2D norm  = new Texture2D( textureSize, textureSize );
   tex.filterMode  = FilterMode.Trilinear;
   norm.filterMode = FilterMode.Trilinear;

   float rGrain = tileBase.r * st.grainRangeMax + (1 - tileBase.r) * st.grainRangeMin;
   float gGrain = tileBase.g * st.grainRangeMax + (1 - tileBase.g) * st.grainRangeMin;
   float bGrain = tileBase.b * st.grainRangeMax + (1 - tileBase.b) * st.grainRangeMin;

   /* rcom = 0.299
      gcom = 0.587
      bcom = 0.114
      */

   Color[] pixels = new Color[ textureSize * textureSize ];
   float[] bump   = new float[ textureSize * textureSize ];

   for (int y = 0; y < textureSize; y++) for (int x = 0; x < textureSize; x++) {

      float grain     = noise[ x, y ];
      float bumpGrain = grain * st.bumpNoise;

      for (int i = 0; i < numLayers; i++) {

         grain     += perlinLayers[ i ][ x + overlap, y + overlap ] * st.perlin * Mathf.Pow( st.balance,     i );
         bumpGrain += perlinLayers[ i ][ x + overlap, y + overlap ] * st.perlin * Mathf.Pow( st.bumpBalance, i );
      }
      Color col = new Color( tileBase.r + grain * rGrain,
                             tileBase.g + grain * gGrain,
                             tileBase.b + grain * bGrain, 1.0f ) * st.multiply;

      bump[ y * textureSize + x ] = ((tileBase.r + bumpGrain * rGrain) * 0.299f
                                  +  (tileBase.g + bumpGrain * gGrain) * 0.587f
                                  +  (tileBase.b + bumpGrain * bGrain) * 0.114f) * st.multiply;

      pixels[ y * textureSize + x ] = col;
   }
   tex.SetPixels( pixels );
   tex.Apply();

   Color[] normPx = new Color[ textureSize * textureSize ];
   max = textureSize * textureSize;

   for (int y = 0; y < textureSize; y++) for (int x = 0; x < textureSize; x++) {

      int step = textureSize;
      int c    = y * textureSize + x;

      float xn = bump[ loop( c - 1 + step ) ] *  3.0f - bump[ loop( c + 1 + step ) ] *  3.0f
               + bump[ loop( c - 1        ) ] * 10.0f - bump[ loop( c + 1        ) ] * 10.0f
               + bump[ loop( c - 1 - step ) ] *  3.0f - bump[ loop( c + 1 - step ) ] *  3.0f;

      float yn = bump[ loop( c - step - 1 ) ] *  3.0f - bump[ loop( c + step - 1 ) ] *  3.0f
               + bump[ loop( c - step     ) ] * 10.0f - bump[ loop( c + step     ) ] * 10.0f
               + bump[ loop( c - step + 1 ) ] *  3.0f - bump[ loop( c + step + 1 ) ] *  3.0f;

      xn               *= 0.25f * st.bumpStrength;
      yn               *= 0.25f * st.bumpStrength;
      const float zn    = 1.0f;
      Vector3 normalVec = new Vector3( xn, yn, zn ).normalized;
      //normPx[ c ]       = new Color( 0.0f, normalVec.y * 0.5f + 0.5f, 0.0f, normalVec.x * 0.5f + 0.5f );
      normPx[ c ]       = new Color( normalVec.x * 0.5f + 0.5f, normalVec.y * 0.5f + 0.5f, zn, 1.0f );
   }
   norm.SetPixels( normPx );
   norm.Apply();

   Material material = new Material( prototypeMat ) { mainTexture = tex };
   material.SetTexture( "_BumpMap", norm );

   //print( material );

   return new Object[] { tex, norm, material };
}

public void SaveTexture( Texture2D tex, Texture2D norm, string ID ) {

   byte[] pngData = tex.EncodeToPNG();
   File.WriteAllBytes( "Assets/Resources/texture-" + ID + ".png", pngData );
   pngData = norm.EncodeToPNG();
   File.WriteAllBytes( "Assets/Resources/normal-" + ID + ".png", pngData );
}

public void GenerateTextures() {

   altDistribution   = new int[ altDivs ];
   slopeDistribution = new int[ slopeDivs ];
   numInstances      = new int[ altDivs, slopeDivs ];
   altIntervals      = new float[ altDivs - 1 ];
   slopeIntervals    = new float[ slopeDivs - 1 ];
   texs              = new List< Texture2D >();
   float startTime   = Time.realtimeSinceStartup;

   noise = new float[ textureSize, textureSize ];
   for (int i = 0; i < textureSize; i++) for (int j = 0; j < textureSize; j++) {
      noise[ i, j ] = Random.Range( -1.0f, 1.0f );
   }

   perlinLayers = new float[ numLayers ][ , ];
   int rawSize  = (int) (textureSize * (1 + overlapAmount));
   overlap      = rawSize - textureSize;

   for (int l = 0; l < numLayers; l++) {  // generate perlin layer base pixels

      perlinLayers[ l ] = new float[ rawSize, rawSize ];
      Vector2 offset    = new Vector2( Random.Range( 0.0f, 100.0f ), Random.Range( 0.0f, 100.0f ) );

      for (int x = 0; x < rawSize; x++) for (int y = 0; y < rawSize; y++) {

         float   pScale = 3.0f * Mathf.Pow( 2.0f, l );
         Vector2 coords = new Vector2( offset.x + x / pScale, offset.y + y / pScale );
         perlinLayers[ l ][ x, y ] = Mathf.PerlinNoise( coords.x, coords.y ) - 0.5f;

         if (x > textureSize) {

            float amount = SinRamp( (x - textureSize) / (float) overlap );
            perlinLayers[ l ][ x, y ] = (1 - amount) * perlinLayers[ l ][ x, y ]
                                           + amount  * perlinLayers[ l ][ x - textureSize, y ];
         }
         if (y > textureSize) {

            float amount = SinRamp( (y - textureSize) / (float) overlap );
            perlinLayers[ l ][ x, y ] = (1 - amount) * perlinLayers[ l ][ x, y ]
                                           + amount  * perlinLayers[ l ][ x, y - textureSize ];
         }
      }
   }

   Object[] lowFlat   = GenerateBase( styles[ useStyle ].lowFlatCol );
   Object[] lowSlope  = GenerateBase( styles[ useStyle ].lowSlopeCol );
   Object[] highFlat  = GenerateBase( styles[ useStyle ].highFlatCol );
   Object[] highSlope = GenerateBase( styles[ useStyle ].highSlopeCol );

   Texture2D lowFlatTex   = (Texture2D) lowFlat[ 0 ];
   Texture2D lowSlopeTex  = (Texture2D) lowSlope[ 0 ];
   Texture2D highFlatTex  = (Texture2D) highFlat[ 0 ];
   Texture2D highSlopeTex = (Texture2D) highSlope[ 0 ];

   Texture2D lowFlatNorm   = (Texture2D) lowFlat[ 1 ];
   Texture2D lowSlopeNorm  = (Texture2D) lowSlope[ 1 ];
   Texture2D highFlatNorm  = (Texture2D) highFlat[ 1 ];
   Texture2D highSlopeNorm = (Texture2D) highSlope[ 1 ];

   SaveTexture( lowFlatTex,   lowFlatNorm,   "0-0"                                            );
   SaveTexture( lowSlopeTex,  lowSlopeNorm,  "0-"                     + (slopeDivs - 1)       );
   SaveTexture( highFlatTex,  highFlatNorm,  (altDivs - 1) + "-0"                  );
   SaveTexture( highSlopeTex, highSlopeNorm, (altDivs - 1) + "-" + (slopeDivs - 1) );

   texs.Add( lowFlatTex );
   texs.Add( lowSlopeTex );
   texs.Add( highFlatTex );
   texs.Add( highSlopeTex );

   if (!useNewShader) {

      // generate intermediate textures through interpolation
      for (int a = 0; a < altDivs; a++) for (int s = 0; s < slopeDivs; s++) {

         if ((a == 0 || a == altDivs - 1) && (s == 0 || s == slopeDivs - 1)) { continue; }

         Texture2D tex     = new Texture2D( textureSize, textureSize );
         Texture2D norm    = new Texture2D( textureSize, textureSize );
         float     aWeight = (float) (a) / (altDivs - 1);
         float     sWeight = (float) (s) / (slopeDivs - 1);
         Color[]   pixels  = new Color[ textureSize * textureSize ];
         Color[]   bump    = new Color[ textureSize * textureSize ];

         //print("alt: " + a + ", slope: " + s + ", aWeight: " + aWeight + ", sWeight: " + sWeight );

         for (int x = 0; x < textureSize; x++) for (int y = 0; y < textureSize; y++) {

            pixels[ x + y * textureSize ] = sWeight  * (aWeight  * highSlopeTex.GetPixel( x, y )
                                                 + (1 - aWeight) *  lowSlopeTex.GetPixel( x, y ))
                                     + (1 - sWeight) * (aWeight  *  highFlatTex.GetPixel( x, y )
                                                 + (1 - aWeight) *   lowFlatTex.GetPixel( x, y ));

            bump[ x + y * textureSize ] = sWeight  * (aWeight  * highSlopeNorm.GetPixel( x, y )
                                               + (1 - aWeight) *  lowSlopeNorm.GetPixel( x, y ))
                                   + (1 - sWeight) * (aWeight  *  highFlatNorm.GetPixel( x, y )
                                               + (1 - aWeight) *   lowFlatNorm.GetPixel( x, y ));
         }
         tex.SetPixels( pixels );
         tex.Apply();
         norm.SetPixels( bump );
         norm.Apply();
         texs.Add( tex );
         SaveTexture( tex, norm, a + "-" + s );
      }
   }
   print( "Time to generate textures: " + (Time.realtimeSinceStartup - startTime) + " seconds" );
}

void Start() {

}

public void CalcDistributions( float[] alts, float[] slopes ) {

   System.Array.Sort( alts   );
   System.Array.Sort( slopes );

   for (int i = 0; i < altDivs - 1; i++) {

      altIntervals[ i ] = alts[ (int) ((alts.Length / (float) altDivs) * (i + 1)) ];
      //print( "altInterval " + i + ": " + altIntervals[ i ] );
   }
   prototypeMat.SetFloat( "_min_altitude", alts[ 0 ] );
   prototypeMat.SetFloat( "_max_altitude", alts[ alts.Length-1 ] );
   for (int i = 0; i < slopeDivs - 1; i++) {

      slopeIntervals[ i ] = slopes[ (int) ((slopes.Length / (float) slopeDivs) * (i + 1)) ];
   }
}

public Material GetMaterial( float alt, float slope, bool count ) {

   if (useNewShader) { return prototypeMat; }

   int a = 0, s = 0;

   while (a < altDivs   - 1 && alt   >= altIntervals  [ a ]) { a++; }
   while (s < slopeDivs - 1 && slope >= slopeIntervals[ s ]) { s++; }

   if (count) {
      altDistribution[ a ]++;
      slopeDistribution[ s ]++;
      numInstances[ a, s ]++;
   }
   return mat[ a, s ];
}

public void ShowStats() {

   StringBuilder asb = new StringBuilder();
   asb.Append( "Altitude distribution: " );
   for (int a = 0; a < altDivs; a++) { asb.Append( a + ": " + altDistribution[ a ] + ", " ); }
   print( asb );

   StringBuilder ssb = new StringBuilder();
   ssb.Append( "Slope distribution: " );
   for (int s = 0; s < slopeDivs; s++) { ssb.Append( s + ": " + slopeDistribution[ s ] + ", " ); }
   print( ssb );

   /*for (int a = 0; a < altDivs; a++) {

      StringBuilder sb = new StringBuilder();
      sb.Append( "Altitude " + a + ": " );
      for (int s = 0; s < slopeDivs; s++) { sb.Append( numInstances[ a, s ].ToString() + ", " ); }
      print( sb );
   }*/
}

void Update() {

}

}
