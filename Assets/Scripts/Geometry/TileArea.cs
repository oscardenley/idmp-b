﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class TileArea {

   public HashSet< Tile > tiles  = new HashSet< Tile >();
   public HashSet< Tile > border = new HashSet< Tile >();

   public virtual bool Add( Tile newTile ) {

      tiles.Add( newTile );
      newTile.parentArea = this;
      foreach (Tile n in newTile.neighbours) { border.Add( n ); }
      border.ExceptWith( tiles );
      return true;
   }

   public virtual bool Merge( TileArea area ) {

      foreach (Tile t in area.tiles) { Add( t ); }
      return true;
   }
}