﻿// Hex struct and methods
// Written by Samuel Crisp
// Based on the model described at http://www.redblobgames.com/grids/hexagons/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public struct Hex {
	private int _q, _r;

	public enum Direction {
		East = 0,
		Southeast = 1,
		Southwest = 2,
		West = 3,
		Northwest = 4,
		Northeast = 5
	}

	public interface IPoint {
		int q {get;}
		int r {get;}

		Hex Axial();
	}

	public class OddRPoint : IPoint {
		public int q {get; private set;}
		public int r {get; private set;}

		public OddRPoint(int q, int r)
		{
			this.q = q;
			this.r = r;
		}

		public OddRPoint(Hex h) {
			this.q = h.x + (h.z - (h.z & 1)) / 2;
			this.r = h.z;
		}

		public Hex Axial() {
			return new Hex(q-(r - (r & 1)) /2, q);
		}

		public override string ToString ()
		{
			return string.Format ("[Odd-R coordinate: q={0}, r={1}]", q, r);
		}
	}

	// Cube coordinates
	public int x {
		get {
			return _q;
		}
	}
	public int y {
		get {
			return -x-z;
		}
	}
	public int z {
		get {
			return _r;
		}
	}

	// Axial coordinates
	public int q {
		get {
			return _q;
		}
		set {
			_q = value;
		}
	}
	public int r {
		get {
			return _r;
		}
		set {
			_r = value;
		}
	}

	// Constructors
	public Hex(int q, int r) {
		this._q = q;
		this._r = r;
	}

	public Hex(int x, int y, int z) {
		this._q = x;
		this._r = z;
	}

	public Hex(OddRPoint p) {
		this._q = p.q - (p.r - (p.r&1)) / 2;
		this._r = p.r;
	}

	public Hex Scale(int size){
		return new Hex(q*size, r*size);
	}

   public int CubeDistance( Hex other ) {

      return Mathf.Abs( x - other.x )
           + Mathf.Abs( y - other.y )
           + Mathf.Abs( z - other.z );
   }

	// Get neighbour
	public Hex Neighbour(Hex.Direction dir, int magnitude = 1) {
		Hex[] neighbours = new Hex[6] {
			new Hex(1,0),
			new Hex(1,-1),
			new Hex(0,-1),
			new Hex(-1,0),
			new Hex(-1,1),
			new Hex(0,1)
		};
		Hex d = neighbours[(int)dir];
		return this + d * magnitude;
	}

	public Hex[] Neighbours {
		get {
			Hex[] neighbours = new Hex[6];
			foreach (Direction dir in (Direction[]) Enum.GetValues(typeof(Direction))) {
				neighbours[(int)dir] = this.Neighbour(dir);
			}
			return neighbours;
		}
	}

	// Return an array of all hexes within a range
	// This method is currently broken, use SpiralRings() instead
	// it has the same intended functionality but is slightly less efficient
	public Hex[] Range(int size){
		List<Hex> range = new List<Hex>();
		if (size < 0) return range.ToArray();
		else if (size == 0) {
			range.Add (this);
			return range.ToArray();
		}

		int xmin = this.x - size;
		int xmax = this.x + size;
		int ymin = this.y - size;
		int ymax = this.y + size;
		int zmin = this.z - size;
		int zmax = this.z + size;

		for (int x = xmin; x <= xmax; x++) {
			for (int y = Mathf.Max(ymin, -x-zmax); y <= Mathf.Min(ymax, -x-zmin); y++) {
				int z = -x-y;
				range.Add(new Hex(x, y, z));
			}
		}
		/*
		for (int dX=-size; dX<size; dX++)
		{
			for (int dY= Mathf.Max (-size, -dX-size); dY<= Mathf.Min (size, -dX+size); dY++) 
			{
				int dZ = -dX-dY;
				range.Add(this + new Hex(dX,dY,dZ));
			}
		} */
		return range.ToArray();
	}

	public Hex[] SpiralRings(int size) {
		List<Hex> range = new List<Hex>();
		range.Add (this);
		for (int k=1; k<= size; k++)
		{
			Hex h = this + new Hex(-1,1) * k;
			for (int i=0; i< 6; i++)
			{
				for (int j=0; j< k; j++)
				{
					range.Add(h);
					h = h.Neighbour((Direction)i);
				}
			}
		}
		return range.ToArray();
	}

	public Hex[] Ring (int size)
	{
		List<Hex> ring = new List<Hex>();
		Hex h = this + new Hex(-1,1) * size;
		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < size; j++)
			{
				ring.Add (h);
				h = h.Neighbour((Direction)i);
			}
		}
		return ring.ToArray();
	}

	public Hex[] LineTo(Hex to, int range = 0) {
		return Hex.Line (this, to, range);
	}

	public Hex[] LineTo(float dir, float magnitude, int range = 0)
	{
		return Hex.Line (this, dir, magnitude, range);
	}

	public static Hex[] Line(Hex start, Hex end, int range = 0) {
		List<Hex> line = new List<Hex>();
		int n = Hex.Distance(start, end);
		float startX = start.x + 0.0001f;
		float startY = start.y + 0.0001f;
		float startZ = start.z + 0.0001f;
		for (int i=0; i <= n; i ++) {
			float x = startX * (1.0f- (float)i/n) + end.x * (float)i/n;
			float y = startY * (1.0f- (float)i/n) + end.y * (float)i/n;
			float z = startZ * (1.0f- (float)i/n) + end.z * (float)i/n;
			line.Add (Hex.Round(x,y,z));
		}
		if (range != 0) return Hex.AllWithinRange(line.ToArray(), range);
		else return line.ToArray();
	}

	public static Hex[] Line(Hex start, float dir, float magnitude, int range = 0)
	{
		return Hex.Line (start, start.AngleOffset(dir, magnitude), range);
	}

	public Hex RandomNeighbour {
		get {
			return this.Neighbour((Direction) UnityEngine.Random.Range(0,5));
		}
	}

	// Takes in a set of Hexes and returns their difference about a center
	public static Hex[] Normalise (Hex[] set, Hex center) {
		Hex[] result = new Hex[set.Length]; 
		Array.Copy(set,result, set.Length);
		for (int i=0; i <= set.Length; i++) {
			result[i] = result[i] - center;
		}
		return result;
	}

	// Get distance between Hexes
	public static int Distance (Hex a, Hex b) {
		return (Mathf.Abs(a.q - b.q) + Mathf.Abs(a.r - b.r) + Mathf.Abs(a.q + a.r - b.q - b.r)) / 2;
	}

	public static Hex[] AllNeighbours (Hex[] hexes) {
		List<Hex> allneighbours = new List<Hex>();
		foreach (Hex h in hexes)
		{
			foreach (Hex i in h.Neighbours) 
			{
				if (!allneighbours.Contains(i)) allneighbours.Add (i);
			}
		}
		return allneighbours.ToArray();
	}

	public static Hex[] AllWithinRange (Hex[] hexes, int range) {
		List<Hex> allwithinrange = new List<Hex>();
		foreach (Hex h in hexes)
		{
			foreach (Hex i in h.Range (range)) 
			{
				if (!allwithinrange.Contains(i)) allwithinrange.Add (i);
			}
		}
		return allwithinrange.ToArray();
	}


	public Hex AngleOffset (float dir, float magnitude) 
	{
		Vector3 offset = Quaternion.AngleAxis(dir, Vector3.up) * Vector3.right * magnitude;
		return this + Hex.WorldPointToHex(offset);
	}

	public static Hex WorldPointToHex(Vector3 point, float size = 1.0f)
	{
		float q = (1.0f/3.0f*Mathf.Sqrt(3.0f) * point.x - 1.0f/3.0f * point.z) / size;
		float r = 2.0f/3.0f * point.z / size;
		return Hex.Round(q,r);
	}

	public static Hex Round (float q, float r) {
		int rq = Convert.ToInt32(System.Math.Round(q, MidpointRounding.AwayFromZero));
		int rr = Convert.ToInt32(System.Math.Round(r, MidpointRounding.AwayFromZero));
		return new Hex(rq, rr);
	}

	public static Hex Round (float x, float y, float z) {
		int rx = Convert.ToInt32(System.Math.Round(x, MidpointRounding.AwayFromZero));
		int ry = Convert.ToInt32(System.Math.Round(y, MidpointRounding.AwayFromZero));
		int rz = Convert.ToInt32(System.Math.Round(z, MidpointRounding.AwayFromZero));

		float x_diff = Mathf.Abs(rx - x);
		float y_diff = Mathf.Abs(ry - y);
		float z_diff = Mathf.Abs(rz - z);

		if (x_diff > y_diff && x_diff > z_diff) {
			rx = -ry-rz;
		}
		else if (y_diff > z_diff) {
			ry = -rx-rz;
		}
		else {
			rz = -rx-ry;
		}
		return new Hex(rx, ry, rz);
	}

	// Get OddR coordinate
	public OddRPoint OddR {
		get {
			return new OddRPoint(this);
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Hex: q={0}, r={1}]", q, r);
	}

	public override bool Equals (object obj)
	{
		if (!(obj is Hex)) return false;
		Hex hex = (Hex) obj;
		return q==hex.q && r==hex.r;
	}

	public override int GetHashCode ()
	{
		return base.GetHashCode ();
	}

	public static bool operator ==(Hex x, Hex y) 
	{
		return x.q == y.q && x.r == y.r;
	}
	public static bool operator !=(Hex x, Hex y) 
	{
		return !(x == y);
	}
	public static Hex operator +(Hex h1, Hex h2)
	{
		return new Hex(h1.q + h2.q , h1.r + h2.r);
	}
	public static Hex operator -(Hex h1, Hex h2)
	{
		return new Hex(h1.q - h2.q , h1.r - h2.r);
	}
	public static Hex operator *(Hex h, int scalar)
	{
		return h.Scale(scalar);
	}
}