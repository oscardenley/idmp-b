﻿using UnityEngine;
using System.Collections;

public class DeformationPower : MonoBehaviour {

   public string powerName;
   public AnimationCurve startCurve;
   public float startPeriod;
   public AnimationCurve endCurve;
   public float endPeriod;
   public float scale;
   public float maxSustainTime;

   void Start() {

   }

   void Update() {

   }

   public Deformation StartDeformation( bool inverse = false ) {

      return new Deformation( startCurve, scale, startPeriod, inverse );
   }

   public Deformation EndDeformation( bool inverse = false ) {

      return new Deformation( endCurve, scale, endPeriod, inverse );
   }

}
