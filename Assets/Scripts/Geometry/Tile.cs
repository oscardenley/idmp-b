using System.Text;
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class Tile : MonoBehaviour {

   [System.Serializable]
   public class Corner {

      public static int[] totalNumAdjacent = new int[10];

      public Vector2 pos;        // corners are identified by a vector2 - height difference is ignored
      public float   h;
      public int     meshVertexIndex;
      //public Vector3 cachedNormal;  // this is used for calculating average normal among neighbours
      public Tile[]  neighbours;  // up to two neighbour tiles that share this corner with this tile
      Tile           tile;        // this tile
      bool[]         countNormals = { false, false, false };  // self, neighbour[0], neighbour[1]

      public Corner( Tile t, Vector3 p, Tile[] n ) {

         tile       = t;
         pos        = p;
         neighbours = n;
         h          = t.height;  // initially set corner height to tile height
         CalculateHeight();
         totalNumAdjacent[ n.Length ]++;
      }

      public void CalculateHeight() {  // similar to the adjTileHeight method for hex tiles

         if (neighbours.Length < 2) { return; }  // don't bother if less than two neighbours

         if (neighbours.Length == 3) {

            h = (tile.height + neighbours[0].height + neighbours[1].height + neighbours[2].height) / 4.0f;
            return;
         }

         /* New procedure, which is not biased towards any corner:
          * List the three corners.
          * Find which pair has the shortest distance between them.
          * If this distance is greater than the edge threshold, return the tile height.
          * Otherwise merge those two heights.
          * If the distance between the merged height and the other height is greater than threshold,
          * return whichever height is attached to this corner.
          * Otherwise return the average of all heights.
          */
         float[] heights = { tile.height, neighbours[0].height, neighbours[1].height };

         float dist01      = Mathf.Abs( heights[1] - heights[0] );
         float dist02      = Mathf.Abs( heights[2] - heights[0] );
         float dist12      = Mathf.Abs( heights[2] - heights[1] );
         float closestDist = dist01;
         int   outlier     = 2;
         if (dist02 < dist01)                    { outlier = 1; closestDist = dist02; }
         if (dist12 < dist02 && dist12 < dist01) { outlier = 0; closestDist = dist12; }

         if (closestDist > tile.edgeThreshold) {  // set normal and height to that of tile centre
         
            countNormals = new[] { true, false, false };
            return;
         }

         // merge the two closest heights
         float merged     = (heights[0] + heights[1] + heights[2] - heights[ outlier ]) / 2.0f;
         float distmerged = Mathf.Abs( heights[ outlier ] - merged );

         if (distmerged > tile.edgeThreshold) {
         
            h = outlier == 0 ? heights[0] : merged;
            countNormals = outlier == 0 ? new[] { true, false, false }
                         : outlier == 1 ? new[] { true, false, true  }
                                        : new[] { true, true,  false };
            return;
         }

         h            = (heights[0] + heights[1] + heights[2]) / 3.0f;
         countNormals = new[] { true, true, true };
      }
      public void CalculateNormal() {

         Vector3 sum = tile.norms[0];
         int     num = 1;

         for (int i = 0; i < 3 && i < neighbours.Length; i++) /*if (countNormals[i+1])*/ {

            sum += neighbours[i].cachedNormal;
            num++;
         }
         tile.norms[ meshVertexIndex ] = sum / num;
      }
   }

   class DeformPowerInstance {

      public DeformationPower power;
      public Timer            t;

      public DeformPowerInstance( DeformationPower p, float time ) {

         power = p;
         t     = new Timer( time ).Start();
      }
   }
                     static float                           sin60 = Mathf.Sin( 60.0f * Mathf.Deg2Rad );
   [HideInInspector] public float                           height;
   [HideInInspector] public float                           textureHeight;
   [HideInInspector] public Vector3                         cachedNormal;
   [HideInInspector] public float                           finalHeight;
   [HideInInspector] public float                           slope = 0.0f;
                     public float                           cursorHeight;
                     public float                           patternHeight;
                     public float                           dpatternHeight;
                     public Hex                             position;
                     public List< Tile >                    neighbours = new List< Tile >();
                     public List< Tile >                    adjacentWaterBorder = new List< Tile >();
   [HideInInspector] public bool                            outside;
                     public bool                            voronoi;
                     public GameObject                      cursor;
                     static Tile                            highlightedTile;
                     public float                           edgeThreshold = 1.5f;
                     public float                           returnSpeed;
                     public float                           uvScale = 12.5f;
                     public float                           depressHeight;
                     public Vector2[]                       cornerPoints;  // public so neighbours can work out which vertices are shared
   [HideInInspector] public float                           lowestTopPoint  = 0.0f;
   [HideInInspector] public float                           highestTopPoint = 0.0f;
                     public Vector2                         centroid;  // subtract this from corner points to generate mesh
                     public List< float >                   edgePositions;
                     public List< Corner >                  corners            = new List< Corner >();
                            List< Deformation >             activeDeformations = new List< Deformation >();
                            List< DeformPowerInstance >     activeDeformPowers = new List< DeformPowerInstance >();
                     public float                           pathUpdateInterval;
                     static Timer                           pathUpdate;
   [HideInInspector] public bool                            needUpdate = false;
   [HideInInspector] public bool                            occupied  = false;  // an enemy or scenery item has been placed here already
   [HideInInspector] public float                           sceneryWeight = 0.0f;
   [HideInInspector] public Transform                       sceneryProp;
                     public float                           resistance;
   [HideInInspector] public bool                            underwater  = false;
   [HideInInspector] public bool                            markForRemoval = false;
                            bool                            returning;
                     public Color                           cursorDefaultColor;
                     public Color                           cursorFairyPlacementColor;
                     public Color                           cursorDeformColor;
                     public Color                           cursorMoveColor;
                     public Color                           glowColor;
                     public Color                           dglowColor;
                     public TileArea                        parentArea = null;
                            Mesh                            targetMesh;
                            MeshFilter                      mf, cmf, pmf, dmf;
                            Transform                       cursorTf;
                     public Transform                       waterBorder;
   [HideInInspector] public Transform                       tf, atf, pattern, dpattern;
                            Vector2[]                       uvs;
                            Vector3[]                       norms;
                            int[]                           tris;
                            MeshCollider                    mc;
                            Dictionary< InputState, Color > cursorColors;
                            InputState                      cursorState = InputState.DEFAULT;
                            Vector3                         cachedPos;
                     public TerrainSound                    terrainSound;
                     public float                           terrainSFXvol;
                            //Timer                           distanceCheck = new Timer( Random.Range( 1.0f, 2.5f ) );
                     public float                           displayDistance;

   public float highestPoint { get { return corners.Max( corner => corner.h ); } }

   public void Glow( bool glow, float amount = 0.0f ) {

      if (!pattern) { return; }
      pattern.renderer.enabled        = glow;
      if (!glow) { return; }
      pattern.renderer.material.color = glow ? glowColor * new Color( 1.0f, 1.0f, 1.0f, amount ) : Color.clear;
   }

   public void Indicate( Tile t ) {

      Debug.DrawLine( tf.position          + Vector3.up * 2.0f,
                      t.transform.position + Vector3.up * 2.0f,
                      new Color( 1.0f, 0.7f, 0.0f, 1.0f ), 1.0f );
   }

   public void Highlight( InputState state ) {

      if (state == InputState.FAIRY_HIGHLIGHT) { cursor.renderer.enabled = false; return; }

      if (highlightedTile != this || cursorState != state) {

         cursorState = state;
         if (highlightedTile) { highlightedTile.cursor.renderer.enabled = false; }
         cursor.renderer.enabled        = true;
         cursor.renderer.sharedMaterial.color = cursorColors[ state ];
         highlightedTile                = this;
      }
   }

   public void Depress() {

      atf.localPosition = new Vector3( atf.localPosition.x, depressHeight, atf.localPosition.z );
      returning         = true;
   }

   // generate smoothed normals using each corner's neighbours
   public void CalculateNormals() {

      norms = mf.sharedMesh.normals;
      foreach (Corner c in corners) { c.CalculateNormal(); }
      mf.sharedMesh.normals = norms;
   }

   // generate list of corners, caching links to the tiles that share each corner vertex
   public void CalculateCorners() {

      foreach (Vector2 point in cornerPoints) {  // for each corner vertex
         List< Tile > cornerTiles = new List< Tile >();  // tiles that share this vertex
         foreach (Tile n in neighbours) {  // for each neighbour
            foreach (Vector2 nc in n.cornerPoints) {  // if the neighbour also has the corner
               if ((nc - point).sqrMagnitude < 0.0001f) {
                  cornerTiles.Add( n );  // add the neighbour to this corner's list of sharing tiles
               }
            }
         }
         Corner corner = new Corner( this, point, cornerTiles.ToArray() );
         corners.Add( corner );  // create and add the corner
         if      (corner.h < lowestTopPoint ) { lowestTopPoint  = corner.h; }
         else if (corner.h > highestTopPoint) { highestTopPoint = corner.h; }
      }
   }

   public Tile    lowestNeighbour  { get { return neighbours.OrderBy( neighbour => neighbour.height ).First(); } }
   public Vector3 top              { get { return new Vector3( centroid.x, height, centroid.y ); } }
   public float   deformationDelta { get { return activeDeformations.Sum( d => d.delta ) / (sceneryWeight + 1.0f); } }
   public bool    noDeformations   { get { return activeDeformations.Count == 0; } }

   // immediately start a single deformation
   public void Deform( Deformation d ) {

      activeDeformations.Add( d );
      finalHeight += d.curve.Evaluate( 1.0f ) * d.scale;  // calculate new final height
   }

   // add a timed deformation with a start curve and end curve
   public void Deform( DeformationPower d, float time ) {
   
      dpattern.renderer.enabled = true;
      DeformPowerInstance instance = activeDeformPowers.SingleOrDefault( dpi => dpi.power.powerName == d.powerName );

      if (instance == null) {
      
         Deform( d.StartDeformation() );
         instance = new DeformPowerInstance( d, time );
         activeDeformPowers.Add( instance );
      }
      else { instance.t.period = Mathf.Clamp( instance.t.period + time, 0.0f, d.maxSustainTime ); }
   }

   void UpdateCursorMesh() {

      Mesh      m        = cmf.sharedMesh;
      Mesh      tileMesh = mf.sharedMesh;
      Vector3   hVec     = Vector3.up * height;
      Vector3[] verts    = new Vector3[ cornerPoints.Length + 1 ];
      verts[ 0 ]         = tileMesh.vertices[ 0 ] - hVec;
      for (int i = 1; i <= cornerPoints.Length; i++) { verts[ i ] = tileMesh.vertices[ i ] - hVec; }
      m.vertices         = verts;
      m.RecalculateNormals();
      m.RecalculateBounds();
      m.Optimize();
      cursorTf.position = transform.position + Vector3.up * (height + cursorHeight);
      pattern.position  = transform.position + Vector3.up * (height + patternHeight);
      dpattern.position = transform.position + Vector3.up * (height + dpatternHeight);
   }

   void GenerateCursorMesh() {

      Mesh      m        = new Mesh();
      Mesh      tileMesh = targetMesh;
      Vector3   hVec     = Vector3.up * height;
      Vector3[] verts    = new Vector3[ cornerPoints.Length+1 ];
      verts[0]           = tileMesh.vertices[0] - hVec;
      for (int i = 1; i <= cornerPoints.Length; i++) { verts[i] = tileMesh.vertices[i] - hVec; }

      Vector2[] uv = new Vector2[ cornerPoints.Length + 1 ];
      if (cornerPoints.Length == 6) {  // UV for patterns on hex tiles

         uv[ 0 ] = new Vector2( 0.5f,  0.5f );
         uv[ 1 ] = new Vector2( 1.0f,  0.5f );
         uv[ 2 ] = new Vector2( 0.75f, 0.5f + sin60 / 2.0f );
         uv[ 3 ] = new Vector2( 0.25f, 0.5f + sin60 / 2.0f );
         uv[ 4 ] = new Vector2( 0.0f,  0.5f );
         uv[ 5 ] = new Vector2( 0.25f, 0.5f -(sin60 / 2.0f) );
         uv[ 6 ] = new Vector2( 0.75f, 0.5f -(sin60 / 2.0f) );
      }
      else {
         uv[ 0 ] = tileMesh.uv[ 0 ];
         for (int i = 1; i <= cornerPoints.Length; i++) { uv[ i ] = tileMesh.uv[ i ]; }
      }

      int[] triangles = new int[ cornerPoints.Length * 3 ];
      for (int i = 0; i < cornerPoints.Length * 3; i++) { triangles[i] = tileMesh.triangles[i]; }

      m.vertices  = verts;
      m.uv        = uv;
      m.triangles = triangles;
      m.RecalculateNormals();
      m.Optimize();
      cmf.sharedMesh = pmf.sharedMesh = dmf.sharedMesh = m;
      cursorTf.position = transform.position + Vector3.up * (height + cursorHeight);
      pattern.position  = transform.position + Vector3.up * (height + patternHeight);
      dpattern.position = transform.position + Vector3.up * (height + dpatternHeight);
   }
 
   public void FinaliseMesh() {

      mf.mesh       = targetMesh;
      mc.sharedMesh = targetMesh;
   }

   void UpdateMesh() {
   
      Mesh      m     = mf.sharedMesh;
      Vector3[] verts = m.vertices;
      verts[0]        = new Vector3( 0.0f, height, 0.0f );

      foreach (Corner c in corners) {

         c.CalculateHeight();
         int ind = c.meshVertexIndex;
         verts[ ind ] = new Vector3( verts[ ind ].x, c.h, verts[ ind ].z );  // top for top
         verts[ ind - 1 + (corners.Count + 1) * 2 ] = verts[ ind ];  // top for sides (left)
         verts[ ind - 1 + (corners.Count + 1) * 4 ] = verts[ ind ];  // top for sides (right)
      }
      verts[(corners.Count+1) * 3 - 1] = verts[1];
      verts[(corners.Count+1) * 5 - 1] = verts[1];

      m.vertices = verts;
      m.RecalculateNormals();
      cachedNormal = m.normals[0];
      CalculateNormals();
      m.RecalculateBounds();
      CalculateTangents();
      mc.sharedMesh = null;
      mc.sharedMesh = m;
      UpdateCursorMesh();
      if (!pathUpdate.on) { pathUpdate.Start(); }
   }

   public void CalculateTangents() {

      Mesh      mesh          = mf.sharedMesh;
      int       triangleCount = mesh.triangles.Length / 3;
      int       vertexCount   = mesh.vertices.Length;
      Vector3[] tan1          = new Vector3[ vertexCount ];
      Vector3[] tan2          = new Vector3[ vertexCount ];
      Vector4[] tangents      = new Vector4[ vertexCount ];

      for (int a = 0; a < triangleCount; a += 3) {

         int i1 = mesh.triangles[ a + 0 ];
         int i2 = mesh.triangles[ a + 1 ];
         int i3 = mesh.triangles[ a + 2 ];

         Vector3 v1 = mesh.vertices[ i1 ];
         Vector3 v2 = mesh.vertices[ i2 ];
         Vector3 v3 = mesh.vertices[ i3 ];

         Vector2 w1 = mesh.uv[ i1 ];
         Vector2 w2 = mesh.uv[ i2 ];
         Vector2 w3 = mesh.uv[ i3 ];

         float x1 = v2.x - v1.x;
         float x2 = v3.x - v1.x;
         float y1 = v2.y - v1.y;
         float y2 = v3.y - v1.y;
         float z1 = v2.z - v1.z;
         float z2 = v3.z - v1.z;

         float s1 = w2.x - w1.x;
         float s2 = w3.x - w1.x;
         float t1 = w2.y - w1.y;
         float t2 = w3.y - w1.y;

         float r = 1.0f / (s1 * t2 - s2 * t1);

         Vector3 sdir = new Vector3( (t2 * x1 - t1 * x2) * r,
                                     (t2 * y1 - t1 * y2) * r,
                                     (t2 * z1 - t1 * z2) * r );

         Vector3 tdir = new Vector3( (s1 * x2 - s2 * x1) * r,
                                     (s1 * y2 - s2 * y1) * r,
                                     (s1 * z2 - s2 * z1) * r );
         tan1[ i1 ] += sdir;
         tan1[ i2 ] += sdir;
         tan1[ i3 ] += sdir;

         tan2[ i1 ] += tdir;
         tan2[ i2 ] += tdir;
         tan2[ i3 ] += tdir;
      }
      for (int a = 0; a < vertexCount; ++a) {

         Vector3 n     = mesh.normals[ a ];
         Vector3 t     = tan1[ a ];
         Vector3 tmp   = (t - n * Vector3.Dot( n, t )).normalized;
         tangents[ a ] = new Vector4( tmp.x, tmp.y, tmp.z,
                            ( Vector3.Dot( Vector3.Cross( n, t ), tan2[ a ] ) < 0.0f) ? -1.0f : 1.0f );
      }
      mesh.tangents = tangents;
   }

   public void GenerateMesh( bool newTexture=true ) {

      Mesh            m           = new Mesh();
      int             nc          = corners.Count;
      List< float >   hList       = new List< float > { height };  // list of all heights to consider
      hList.AddRange( corners.Select( c => c.h ) );
      float           mean        = hList.Average();
      slope                       = hList.Select( f => Mathf.Pow( f - mean, 2.0f ) ).Average();
      List< Vector3 > topVerts    = new List< Vector3 > { new Vector3( 0.0f, height, 0.0f ) };
      int             vertexIndex = 1;

      for (int i = nc-1; i >= 0; i--) {

         topVerts.Add( new Vector3( corners[i].pos.x, corners[i].h, corners[i].pos.y )
                     - new Vector3( centroid.x,       0.0f,         centroid.y       ) );
         corners[i].meshVertexIndex = vertexIndex;
         vertexIndex++;
      }

      List< Vector3 > bottomVerts = topVerts.Select( v => new Vector3( v.x, 0.0f, v.z ) ).ToList();
      edgePositions               = new List< float > { 0.0f };

      for (int i = nc-1; i > 0; i--) {

         float thisEdge = (corners[ i-1 ].pos - corners[ i ].pos).magnitude;
         edgePositions.Add( edgePositions[ edgePositions.Count-1 ] + thisEdge );
      }
      float edge = (corners[ nc-1 ].pos - corners[ 0 ].pos).magnitude;
      edgePositions.Add( edgePositions[ edgePositions.Count - 1 ] + edge );

      float           perimiterLength = edgePositions[ edgePositions.Count - 1 ];
      float           roundFactor     = Mathf.Round( perimiterLength / 4.0f ) / perimiterLength;
      edgePositions                   = edgePositions.Select( e => e * roundFactor ).ToList();
      float           uvSize          = 1.0f / uvScale;
      float           uvOffset        = Random.Range( 0.0f, 1.0f );
      List< Vector3 > vertList        = new List< Vector3 >();
      List< Vector2 > uvList          = new List< Vector2 >();

      // top polygon for top
      foreach (Vector3 v in topVerts) {

         vertList.Add( v );
         uvList.Add( (new Vector2( v.x, v.z ) + centroid) * uvSize );
      }
      // bottom polygon for bottom
      foreach (Vector3 v in bottomVerts) {

         vertList.Add( v );
         uvList.Add( (new Vector2( v.x, v.z ) + centroid) * uvSize );
      }
      for (int j = 0; j < 2; j++) {  // once for left edges and once for right edges

         // top polygon for sides
         for (int i = 1; i < topVerts.Count; i++) {

            vertList.Add( topVerts[i] );
            uvList.Add( new Vector2( edgePositions[ i-1 ] + uvOffset, topVerts[i].y / 10.0f ) );
         }
         vertList.Add( topVerts[ 1 ] );
         uvList.Add( new Vector2( edgePositions[ nc ] + uvOffset, topVerts[ 1 ].y / 10.0f ) );

         // bottom polygon for sides
         for (int i = 1; i < bottomVerts.Count; i++) {

            vertList.Add( bottomVerts[ i ] );
            uvList.Add( new Vector2( edgePositions[ i-1 ] + uvOffset, 0.0f ) );
         }
         vertList.Add( bottomVerts[ 1 ] );
         uvList.Add( new Vector2( edgePositions[ nc ] + uvOffset, 0.0f ) );
      }

      m.vertices          = vertList.ToArray();
      m.uv                = uvList.ToArray();
      List< int > triList = new List< int >();
      int         step    = nc+1;

      for (int i = 0; i < nc; i++) {

         triList.Add( 0 );  // triangles for top
         triList.Add( i+1 );
         triList.Add( ((i+1) % nc) + 1 );
      }
      for (int i = 0; i < nc; i++) {

         triList.Add( step*2 + i   );  // triangles for sides
         triList.Add( step*3 + i   );
         triList.Add( step*4 + i+1 );

         triList.Add( step*3 + i   );
         triList.Add( step*5 + i+1 );
         triList.Add( step*4 + i+1 );
      }
      m.triangles = triList.ToArray();

      m.RecalculateNormals();
      cachedNormal = m.normals[0];
      m.Optimize();
      targetMesh = m;
      if (newTexture) { FinaliseMesh(); }
      CalculateTangents();
      GenerateCursorMesh();
      norms = mf.sharedMesh.normals;
   }

   public void SetMaterial( Material mat ) {

      atf.renderer.sharedMaterial = mat;
   }

   public void Init( Transform p, float h, float th, bool o, Vector2 c, Vector2[] cp ) {
      
      transform.parent = p;
      height           = h;
      textureHeight    = th;
      outside          = o;
      centroid         = c;
      cornerPoints     = cp;
   }

   void Awake() {

      tf         = transform;
      atf        = tf.Find("appearance");
      pattern    = tf.Find("pattern");
      dpattern   = tf.Find("dpattern");
      mf         = atf.GetComponent< MeshFilter >();
      mc         = GetComponent< MeshCollider >();
      cursorTf   = cursor.transform;
      cmf        = cursor.GetComponent< MeshFilter >();
      mc.enabled = false;
      pathUpdate = new Timer( pathUpdateInterval );
      cachedPos  = tf.position;
      if ( pattern) { pmf =  pattern.GetComponent< MeshFilter >(); }
      if (dpattern) { dmf = dpattern.GetComponent< MeshFilter >(); }

      cursorColors = new Dictionary< InputState, Color > {

         { InputState.DEFAULT,         cursorDefaultColor        },
         { InputState.DEFORM_DRAG,     cursorDeformColor         },
         { InputState.MOVEMENT,        cursorMoveColor           },
         { InputState.FAIRY_PLACEMENT, cursorFairyPlacementColor },
         { InputState.FAIRY_HIGHLIGHT, cursorFairyPlacementColor },
         { InputState.CHANT_DRAG,      cursorDeformColor         }
      };
   }

   void Start() {
   
      cursor.renderer.enabled = false;
      //distanceCheck.Start();
   }

   void Update() {

      //if (Protagonist.currTile.position.CubeDistance( position ) < 40) { return; }

      if (Mathf.Abs( cachedPos.x - Protagonist.tf.position.x) + Mathf.Abs( cachedPos.z - Protagonist.tf.position.z ) > 40.0f
         && activeDeformations.Count == 0 && activeDeformPowers.Count == 0) {
         return;
      }

      // apply deformations
      bool updateNeighbours = false;

      for (int i = 0; i < activeDeformations.Count; i++) {

         needUpdate       = true;
         updateNeighbours = true;

         if (activeDeformations[i].t.stopping) {

            activeDeformations.RemoveAt(i);
            i--;
            continue;
         }
      }
      if (updateNeighbours) { foreach (Tile n in neighbours) { n.needUpdate = true; } }

      dpattern.renderer.enabled = activeDeformPowers.Count > 0;

      // check if any deformation power instances are ending
      for (int i = 0; i < activeDeformPowers.Count; i++) {

         DeformPowerInstance dpi          = activeDeformPowers[i];
         float amount                     = dpi.t.remaining > 0.0f ? dpi.t.remaining / dpi.power.maxSustainTime : 0.0f;
         dpattern.renderer.material.color = dglowColor * new Color( 1.0f, 1.0f, 1.0f, Mathf.Pow( amount, 0.5f ) );

         if (dpi.t.stopping) {
         
            Deform( dpi.power.EndDeformation() );
            activeDeformPowers.RemoveAt(i);
            i--;
            if (activeDeformPowers.Count == 0) { dpattern.renderer.enabled = false; }
            if (terrainSound && dpi.power.powerName != "RaiseFeedback") { terrainSound.StartSound( TerrainSFX.crumble ); }
         }
      }

      // apply depression fade
      if (returning) {

         if (atf.localPosition.y > 0.001f) {

            atf.localPosition = new Vector3( atf.localPosition.x, 0.0f, atf.localPosition.z );
            returning         = false;
         }
         else {
            float y           = Mathf.Lerp( atf.localPosition.y, 0.0f, returnSpeed * Time.deltaTime );
            atf.localPosition = new Vector3( atf.localPosition.x, y, atf.localPosition.z );
         }
      }

      /*if (distanceCheck.stopping && Protagonist.currTile) {

         Hex playerPos = Protagonist.currTile.position;
         int distance = Mathf.Abs( playerPos.q - position.q ) + Mathf.Abs( playerPos.r - position.r );
         if (distance > 20) {
            atf.renderer.enabled = false;
         }
         else { atf.renderer.enabled = true; }
         distanceCheck.Start();
      }*/
   }

   void LateUpdate() {

      if (needUpdate) {

         float dDelta = deformationDelta;
         height      += dDelta;
         if (sceneryProp) { sceneryProp.Translate( 0.0f, dDelta, 0.0f ); }
         if (terrainSound) { terrainSound.soundVol = Mathf.Abs( terrainSFXvol * (dDelta / Time.deltaTime) ); }
         UpdateMesh();
         if (pathUpdate.stopping) { LevelGenerator.TileChanged( collider.bounds ); }
         needUpdate = false;
      }
   }

}
