﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class CloudGenerator : MonoBehaviour {

public static Material mat;
public Material prototypeMat;
public int textureSize = 64;
public int numLayers;
public int useStyle;
public float xSpeed, ySpeed;
public float overlapAmount = 0.1f;
float[][,] perlinLayers;
int overlap;
int max;

[System.Serializable]
public class Style {

   public float perlin; // amount of perlin noise overlayed as a multiple of grain range
   public float multiply; // multiply all generated colours by this amount to avoid blowout
   public float balance; // how much stronger each perlin level is compared to previous
   public float bumpBalance; // as above but for the bump map
   public float bumpStrength; // strength of bump mapping
   public float power;
}

[SerializeField]
Style[] styles;

int loop( int value ) {

   return value <  0   ? max + value
        : value >= max ? value - max
        : value;
}

static float SinRamp( float value ) {

   return 0.5f * Mathf.Sin( (value - 0.5f) * Mathf.PI ) + 0.5f;
}

void Awake() {

}

Object[] GenerateBase() {  // generate texture, normal and material for base

   Style st = styles[ useStyle ];

   Texture2D tex   = new Texture2D( textureSize, textureSize );
   Texture2D norm  = new Texture2D( textureSize, textureSize );
   tex.filterMode  = FilterMode.Trilinear;
   norm.filterMode = FilterMode.Trilinear;

   Color[] pixels = new Color[ textureSize * textureSize ];
   float[] bump   = new float[ textureSize * textureSize ];

   for (int y = 0; y < textureSize; y++) for (int x = 0; x < textureSize; x++) {

      float grain     = 0.5f;
      float bumpGrain = 0.5f;

      for (int i = 0; i < numLayers; i++) {

         grain     += perlinLayers[ i ][ x + overlap, y + overlap ] * st.perlin * Mathf.Pow( st.balance,     i );
         bumpGrain += perlinLayers[ i ][ x + overlap, y + overlap ] * st.perlin * Mathf.Pow( st.bumpBalance, i ) * st.bumpStrength;
      }
      grain = Mathf.Pow( Mathf.Clamp01( grain * st.multiply ), st.power );
      Color col = new Color( 1.0f, 1.0f, 1.0f, grain );
      bump[ y * textureSize + x ] = bumpGrain;
      pixels[ y * textureSize + x ] = col;
      //pixels[ y * textureSize + x ] = Color.blue;
   }
   tex.SetPixels( pixels );
   tex.Apply();

   /*Color[] normPx = new Color[ textureSize * textureSize ];
   max = textureSize * textureSize;

   for (int y = 0; y < textureSize; y++) for (int x = 0; x < textureSize; x++) {

      int step = textureSize;
      int c    = y * textureSize + x;

      float xn = bump[ loop( c - 1 + step ) ] *  3.0f - bump[ loop( c + 1 + step ) ] *  3.0f
               + bump[ loop( c - 1        ) ] * 10.0f - bump[ loop( c + 1        ) ] * 10.0f
               + bump[ loop( c - 1 - step ) ] *  3.0f - bump[ loop( c + 1 - step ) ] *  3.0f;

      float yn = bump[ loop( c - step - 1 ) ] *  3.0f - bump[ loop( c + step - 1 ) ] *  3.0f
               + bump[ loop( c - step     ) ] * 10.0f - bump[ loop( c + step     ) ] * 10.0f
               + bump[ loop( c - step + 1 ) ] *  3.0f - bump[ loop( c + step + 1 ) ] *  3.0f;

      xn               *= 0.25f * st.bumpStrength;
      yn               *= 0.25f * st.bumpStrength;
      const float zn    = 0.5f;
      Vector3 normalVec = new Vector3( xn, yn, zn ).normalized;
      normPx[ c ]       = new Color( 0.0f, normalVec.y * 0.5f + 0.5f, 0.0f, normalVec.x * 0.5f + 0.5f );
   }
   norm.SetPixels( normPx );
   norm.Apply();*/

   Material material = new Material( prototypeMat ) { mainTexture = tex };
   material.mainTextureScale = new Vector2( 2.0f, 2.0f );
   //material.SetTexture( "_BumpMap", norm );

   return new Object[] { tex, norm, material };
}

public void GenerateTextures() {

   float startTime   = Time.realtimeSinceStartup;
   perlinLayers = new float[ numLayers ][ , ];
   int rawSize  = (int) (textureSize * (1 + overlapAmount));
   overlap      = rawSize - textureSize;

   for (int l = 0; l < numLayers; l++) {  // generate perlin layer base pixels

      perlinLayers[ l ] = new float[ rawSize, rawSize ];
      Vector2 offset    = new Vector2( Random.Range( 0.0f, 100.0f ), Random.Range( 0.0f, 100.0f ) );

      for (int x = 0; x < rawSize; x++) for (int y = 0; y < rawSize; y++) {

         float   pScale = 3.0f * Mathf.Pow( 2.0f, l );
         Vector2 coords = new Vector2( offset.x + x / pScale, offset.y + y / pScale );
         perlinLayers[ l ][ x, y ] = Mathf.PerlinNoise( coords.x, coords.y ) - 0.5f;

         if (x > textureSize) {

            float amount = SinRamp( (x - textureSize) / (float) overlap );
            perlinLayers[ l ][ x, y ] = (1 - amount) * perlinLayers[ l ][ x, y ]
                                           + amount  * perlinLayers[ l ][ x - textureSize, y ];
         }
         if (y > textureSize) {

            float amount = SinRamp( (y - textureSize) / (float) overlap );
            perlinLayers[ l ][ x, y ] = (1 - amount) * perlinLayers[ l ][ x, y ]
                                           + amount  * perlinLayers[ l ][ x, y - textureSize ];
         }
      }
   }

   Object[] cloudstuff   = GenerateBase();
   mat              = (Material) cloudstuff[ 2 ];

   print( "Time to generate textures: " + (Time.realtimeSinceStartup - startTime) + " seconds" );
}

void Start() {

   print("CLOUD GEN IS GO");
   GenerateTextures();
   renderer.material = mat;
}

void Update() {

   renderer.material.mainTextureOffset = new Vector2( (Time.time * xSpeed) % 1.0f, (Time.time * ySpeed) % 1.0f );
}

}
