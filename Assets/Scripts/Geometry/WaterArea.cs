﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class WaterArea : TileArea {
   
   int            maxSize;
   public float   waterLevel = 0.0f;
   public bool    hasPool    = false;
   public Vector2 centroid;
   
   public HashSet< Tile > waterTiles = new HashSet< Tile >();
   public List< Tile >   waterBorder;

   public WaterArea( int max ) { maxSize = max; }

   void CheckBorderTile( Tile t ) {
      
      t.adjacentWaterBorder = new List< Tile >();
      foreach (Tile n in t.neighbours) {
         if (waterBorder.Contains( n ) && !n.markForRemoval) { t.adjacentWaterBorder.Add( n ); }
      }
      // remove from border if only one border neighbour
      if (t.adjacentWaterBorder.Count < 2) { t.markForRemoval = true; }

      // remove from border if have two adjacent border neighbours
      if (t.adjacentWaterBorder.Count == 2
         && t.adjacentWaterBorder[0].neighbours.Contains( t.adjacentWaterBorder[1] )) {

         t.markForRemoval = true;
      }
      // if marked for removal, recheck adjacent border tiles
      if (t.markForRemoval) {
         foreach (Tile n in t.adjacentWaterBorder) { CheckBorderTile( n ); }
      }
   }

   public Mesh waterPlane {
      get {
         if (waterBorder == null) { return null; }
         foreach (Tile t in waterBorder) { CheckBorderTile( t ); }
         IEnumerable< Tile > finalBorder = waterBorder.Where( tile => !tile.markForRemoval );
         //Debug.Log("finalBorder count: " + finalBorder.Count().ToString());
         List< Vector2 >     borderVerts = new List< Vector2 >();
         if (finalBorder.Count() == 0) { return null; }
         Tile                current     = finalBorder.First();

         while (true) {

            borderVerts.Add( current.centroid );
            try { current = current.neighbours.First( tile => finalBorder.Contains( tile )
                                                           && !borderVerts.Contains( tile.centroid ) ); }
            catch (Exception) {
               //Debug.Log("Stopped after " + borderVerts.Count + " verts");
               break;
            }
         }
         int[]     inds      = new Triangulator( borderVerts.ToArray() ).Triangulate();
         Vector3[] meshVerts = new Vector3[ borderVerts.Count ];
         for (int i=0; i < meshVerts.Length; i++) {
            meshVerts[i] = new Vector3( borderVerts[i].x - centroid.x, 0.0f,
                                        borderVerts[i].y - centroid.y );
         }
         Mesh mesh = new Mesh { vertices = meshVerts, uv = borderVerts.ToArray(), triangles = inds };
         mesh.RecalculateNormals();
         mesh.RecalculateBounds();
         return mesh;
      }
   }
   
   // if going over limit, lock the water area and water border to current area and border
   public void SetPool() {
      
      if (hasPool) { return; }
      waterTiles          = new HashSet< Tile >( tiles );
      Vector2 positionSum = waterTiles.Aggregate( Vector2.zero, ( current, t ) => current + t.centroid );
      centroid    = positionSum / waterTiles.Count;
      waterBorder = new List< Tile >( border );
      waterLevel  = tiles.Last().height;
      foreach (Tile t in waterTiles) { t.underwater = true; }
      hasPool = true;
   }

   public override bool Add( Tile newTile ) {
      
      if (!hasPool && tiles.Count >= maxSize) { SetPool(); }
      return base.Add( newTile );
   }
   
   public override bool Merge( TileArea area ) {
      
      if (!hasPool && tiles.Count + area.tiles.Count > maxSize) {
         SetPool();
         ((WaterArea)area).SetPool();
         //Debug.Log("Forming pools after merge for area size " + tiles.Count + " and " + area.tiles.Count);
      }
      return base.Merge( area );
   }
}