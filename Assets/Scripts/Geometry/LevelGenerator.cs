using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Pathfinding;
using System.Linq;

public enum TileShape { square, hex, random }

public class LevelGenerator : MonoBehaviour {

   public static float threshold = 0.3f;

   public int numLayers = 5;
   public float noiseScaleIncrement = 2.0f;
   public float heightIncrement = 2.0f;
   public float startScale = 2.0f;  // number of world units per unit of Perlin noise for smallest layer
   public float startHeight = 1.0f;
   public float lowerAmount = 5.0f;
   static float sin60 = Mathf.Sin( 60.0f * Mathf.Deg2Rad );
   public float soundPosition = 10.0f;  // distance from corners to spawn sound units
   static Timer pathUpdate = new Timer( 0.1f );
   public GameObject[] soundUnits;
   public static float lowerEdge = 5000.0f;
   public int numEnemies;
   public bool disableEnemies = false;
   public Transform[] enemies;
   public Transform water;
   public Transform boundsIndicator;
   public Transform boundaryCollider;
   public Material waterMat;

   //public static List< WaterArea > pools = new List< WaterArea >();

   public GameObject prototype;

   [System.Serializable]
   public class SceneryProp {

      public Transform   prop;
      public float       minScale;
      public float       maxScale;
      public float       frequency;
      public float       boundaryFrequency;
      public float       weight;  // adjusted by scale
   }

   [System.Serializable]
   public class PerlinLayer {

      public float          scale;  // feature size in units
      public float          relief = 1.0f;  // at 1.0, relief size is same as feature size
      public AnimationCurve curve;
   }

   [System.Serializable]
   public class Style {

      public string        name;
      public int           mapSize;
      public int           maxPoolSize;
      public int           numPools;
      public float         avgTileSize;
      public float         borderWidth;
      public float         borderHeight;
      public float         sceneryDensity;  // avg scenery objects per tile
      public float         boundarySceneryDensity;
      public float         waterDepth;
      public float         leafDensity;
      public float         embankmentSteepness;
      public float         embankmentRolloff;
      public TileShape     tileShape;
      public PerlinLayer[] layers;
      public SceneryProp[] scenery;
      public Transform     leaf;
   }
   public Style[] styles;
   public int     useStyle;

   TextureGenerator texGen;
   static GridGraph pathGrid;
   Hex              middle;
   float            startTime;
   static Bounds?   updateArea;
   List< Enemy >    enemyInstances = new List< Enemy >();

   public static Dictionary< Vector2, Tile > tilesByPos;
   public static Dictionary< Hex,     Tile > tilesByHex;
   public static Dictionary< Vector2, Hex  > hexesByPos;
   public static List< Tile >                tileList;

   // split a convex polygon into constituent triangles, given the ordered list of perimiter vertices
   public List< Triangle2 > Triangulate( List< Vector2 > perimiter ) {

      List< Triangle2 > tris = new List< Triangle2 >();

      for (int i = 1; i < perimiter.Count - 1; i++) {
         tris.Add( new Triangle2( perimiter[0], perimiter[i], perimiter[i+1] ) );
      }
      return tris;
   }

   public static Vector2 AvgLoc( Vector2[] points ) {

      return points.Aggregate(Vector2.zero, (current, point) => current + point) / points.Length;
   }

   public static Vector2 AvgLoc( List< Vector2 > points ) {

      return points.Aggregate(Vector2.zero, (current, point) => current + point) / points.Count;
   }

   // get centroid of convex polygon, given the ordered list of perimiter vertices
   Vector2 PolygonCentroid( List< Vector2 > perimiter ) {

      float area = 0.0f;  // the total area of the polygon
      Vector2 weightedSum = Vector2.zero;  
      List< Triangle2 > tris = Triangulate( perimiter );

      foreach (Triangle2 tri in tris) {

         area += tri.area;
         weightedSum += tri.centroid * tri.area;
      }
      Vector2 result = weightedSum / area;
      if (float.IsNaN( result.x )) {
         print( "PolygonCentroid returned NaN for perimiter..." );
         foreach (Vector2 v in perimiter) { print( v ); }
      }
      return weightedSum / area;
   }

   void Awake() {
   
      tilesByPos = new Dictionary< Vector2, Tile >();
      tilesByHex = new Dictionary< Hex,     Tile >();
      hexesByPos = new Dictionary< Vector2, Hex  >();
      tileList   = new List< Tile >();
      texGen     = GetComponent< TextureGenerator >();
   }

   void Start() {

      startTime                        = Time.realtimeSinceStartup;
      GameObject      parent           = new GameObject( "Terrain" );
      GameObject      leaves           = new GameObject( "Leaves" );
      GameObject      pools            = new GameObject( "Pools" );
      Vector2[]       seedLocations    = new Vector2[ numLayers ];
      List< Vector2 > v2points         = new List< Vector2 >();
      List< uint >    colors           = new List< uint >();
      Style           s                = styles[ useStyle ];
      lowerEdge                        = s.borderWidth; // set lower edge position for camera
      int             numPoints        = (int)((s.mapSize * s.mapSize) / (s.avgTileSize * s.avgTileSize));
      int             numInnerPoints   = 0;
      float           totalSceneryFreq = 0.0f;
      float           boundSceneryFreq = 0.0f;

      // create boundary colliders
      Transform x1 = Instantiate( boundaryCollider, new Vector3( s.borderWidth / 2.0f, 0.0f, s.mapSize / 2.0f ),
                                  Quaternion.identity ) as Transform;
      x1.localScale = new Vector3( s.borderWidth, 100.0f, s.mapSize );
      Instantiate( x1, new Vector3( s.mapSize - s.borderWidth / 2.0f, 0.0f, s.mapSize / 2.0f ),
                                  Quaternion.identity );
      Transform z1 = Instantiate( boundaryCollider, new Vector3( s.mapSize / 2.0f, 0.0f, s.borderWidth / 2.0f ),
                                  Quaternion.identity ) as Transform;
      z1.localScale = new Vector3( s.mapSize, 100.0f, s.borderWidth );
      Instantiate( z1, new Vector3( s.mapSize / 2.0f, 0.0f, s.mapSize - s.borderWidth / 2.0f ),
                                  Quaternion.identity );

      foreach (SceneryProp sp in s.scenery) { totalSceneryFreq += sp.frequency;
                                              boundSceneryFreq += sp.boundaryFrequency; }

      for (int i = 0; i < s.layers.Length; i++) {
         seedLocations[i] = new Vector2( Random.value, Random.value ) * 100.0f;
      }

      switch (s.tileShape) {

         case TileShape.random: {
            for (int i = 0; i < numPoints; i++) {
               v2points.Add( new Vector2( Random.value * s.mapSize,
                                          Random.value * s.mapSize ) );
               colors.Add( 0 );
            }
            break;
         }
         case TileShape.hex: {

            float zPos     = 0.375f;  // iterate over both hex coordinates and actual position
            bool  offset   = false;
            int   numTiles = 0;

            for (int i = 0; zPos < s.mapSize; i++) {

               float xPos = sin60 * s.avgTileSize * 0.5f;

               for (int j = 0; xPos < s.mapSize; j++) {

                  float offsetAmount = offset ? s.avgTileSize * sin60 : 0.0f;
                  Vector2 point      = new Vector2( xPos + offsetAmount, zPos );
                  v2points.Add( point );
                  colors.Add( 0 );
                  hexesByPos.Add( point, new Hex( new Hex.OddRPoint(i, j) ) );
                  xPos += 2 * sin60 * s.avgTileSize;
                  numTiles++;
               }
               offset = !offset;
               zPos  += 1.5f * s.avgTileSize;
            }
            print("numTiles: " + numTiles);
            break;
         }
         case TileShape.square: {
            for (float xPos = s.avgTileSize / 2.0f; xPos < s.mapSize; xPos += s.avgTileSize) {
               for (float zPos = s.avgTileSize / 2.0f; zPos < s.mapSize; zPos += s.avgTileSize) {
                  v2points.Add( new Vector2( xPos, zPos ) );
                  colors.Add( 0 );
               }
            }
            break;
         }
      }
      Rect area          = new Rect( 0.0f, 0.0f, s.mapSize, s.mapSize );
      Delaunay.Voronoi v = new Delaunay.Voronoi( v2points, colors, area );
      int cellNum        = 0;

      foreach (Vector2 site in v.SiteCoords()) {
      
         List< Vector2 > region     = v.Region( site );
         // remove duplicate corners
         for (int i = 1; i < region.Count; i++) if (region[i] == region[i-1]) { region.RemoveAt( i ); i--; }
         Vector2 centroid = PolygonCentroid( region );

         if (float.IsNaN( centroid.x )) {

            print( "NaN position on site: " + site + " with centroid: " + centroid + " at hex: " + hexesByPos[ site ] );
            continue;
         }
         Vector3   pos        = new Vector3( centroid.x, 0.0f, centroid.y );
         Vector3[] flatPoints = new Vector3[ region.Count ];  // tile corners in 3D space

         for (int i = 0; i < region.Count; i++) {
            flatPoints[i] = new Vector3( region[i].x, 0.0f, region[i].y );
         }
         float height  = 0.0f;
         bool  outside = false;

         for (int i = 0; i < s.layers.Length; i++) {  // get height with layers of perlin noise

            PerlinLayer l     = s.layers[i];
            float noiseX      = seedLocations[i].x + centroid.x / l.scale;
            float noiseZ      = seedLocations[i].y + centroid.y / l.scale;
            float val         = Mathf.PerlinNoise( noiseX, noiseZ );
            val               = val * l.curve.Evaluate( val );
            float layerHeight = val * l.scale * l.relief * 0.5f;
            height           += layerHeight;
         }
         float textureHeight = height;

         if (centroid.x < s.borderWidth || centroid.x >= s.mapSize - s.borderWidth ||
             centroid.y < s.borderWidth || centroid.y >= s.mapSize - s.borderWidth) {
         
            //height += s.borderHeight;
            outside     = true;
            float distX = centroid.x < s.borderWidth ? s.borderWidth - centroid.x : centroid.x + s.borderWidth - s.mapSize;
            float distY = centroid.y < s.borderWidth ? s.borderWidth - centroid.y : centroid.y + s.borderWidth - s.mapSize;
            float dist  = Mathf.Max( distX, distY );
            height     += s.embankmentSteepness * Mathf.Log( 1.0f + dist * s.embankmentRolloff );
         }
         GameObject tile = Instantiate( prototype, pos, Quaternion.identity ) as GameObject;
         tile.name       = "Tile " + cellNum;
         Tile t          = tile.GetComponent< Tile >();
         t.Init( parent.transform, height, textureHeight, outside, centroid, region.ToArray() );
         cellNum++;
         if (!outside) { numInnerPoints++; }

         if (Random.value < (outside ? s.boundarySceneryDensity : s.sceneryDensity)) {

            float selector  = Random.Range( 0.0f, outside ? boundSceneryFreq : totalSceneryFreq );
            int   propIndex = 0;

            while (selector > (outside ? s.scenery[ propIndex ].boundaryFrequency
                                       : s.scenery[ propIndex ].frequency)) {

               selector -= (outside ? s.scenery[ propIndex ].boundaryFrequency
                                    : s.scenery[ propIndex ].frequency);
               propIndex++;
            }
            SceneryProp sp = s.scenery[ propIndex ];
            Transform prop = Instantiate( sp.prop, pos + Vector3.up * height,
                                          Quaternion.Euler( 0.0f, Random.Range( 0, 360.0f ), 0.0f ) ) as Transform;
            float scale     = Random.Range( sp.minScale, sp.maxScale );
            prop.localScale = new Vector3( scale, scale, scale );
            prop.parent     = t.tf;
            t.occupied      = true;
            t.sceneryProp   = prop;
            t.sceneryWeight = sp.weight * scale;
         }
         // make leaves
         if (Random.value < s.leafDensity) {

            Transform leaf  = Instantiate( s.leaf, t.top + Vector3.up, Quaternion.identity ) as Transform;
            float scale     = Random.Range( 0.2f, 0.6f );
            leaf.localScale = new Vector3( scale, scale, scale );
            leaf.parent     = leaves.transform;
         }
         tilesByPos.Add( site, t );
         tileList.Add( t );

         if (s.tileShape == TileShape.hex) {

            t.position = hexesByPos[ site ];
            tilesByHex.Add( t.position, t );
         }
      }

      // now go through and establish links to neighbour tiles, then calculate corners
      foreach (Vector2 vec in tilesByPos.Keys) {  // for every tile
         List< Vector2 > neighbours = v.NeighborSitesForSite( vec );  // get voronoi neighbours
         foreach (Vector2 neighbour in neighbours.Where(neighbour => tilesByPos.ContainsKey( neighbour )))
         {
            // add the corresponding tile to current tile's neighbour list
            tilesByPos[ vec ].neighbours.Add( tilesByPos[ neighbour ] );
         }
         tilesByPos[ vec ].CalculateCorners();
      }
      for (int i = 0; i < Tile.Corner.totalNumAdjacent.Length; i++) {
         //print("total num adjacent #" + i + ": " + Tile.Corner.totalNumAdjacent[i]);
      }
      print("terrain generation time: " + (Time.realtimeSinceStartup - startTime) + " seconds");

      startTime = Time.time;
      // calculate water
      IEnumerable< Tile > tilesByHeight = tilesByPos.Values.Where( tile => !tile.outside ).OrderBy( tile => tile.height );
      List< TileArea >    areas         = new List< TileArea >();
      //float               prevHeight    = 0.0f;

      foreach (Tile t in tilesByHeight) {

         List< TileArea > adjacentAreas = new List< TileArea >();
         foreach (Tile n in t.neighbours.Where( n => n.parentArea != null
                                                  && !adjacentAreas.Contains( n.parentArea ) )) {
            adjacentAreas.Add( n.parentArea );
         }
         switch (adjacentAreas.Count) {

            case 0: {
               WaterArea newArea = new WaterArea( s.maxPoolSize );
               newArea.Add( t );
               areas.Add( newArea );
               break;
            }
            case 1: {
               adjacentAreas[0].Add( t );
               break;
            }
            default: {
               for (int i = 1; i < adjacentAreas.Count; i++) { adjacentAreas[0].Merge( adjacentAreas[i] ); }
               break;
            }
         }
         //prevHeight = t.height;
      }

      IEnumerable< TileArea > poolsBySize = areas.OrderBy( ta => ((WaterArea)ta).waterTiles.Count ).Reverse();
      int                     c           = 0;
      foreach (WaterArea wa in poolsBySize.Cast< WaterArea >()) {

         foreach( Tile t in wa.waterTiles ) { t.underwater = true; }
         Transform w = Instantiate( water, new Vector3( wa.centroid.x, wa.waterLevel, wa.centroid.y ), Quaternion.identity ) as Transform;
         w.GetComponent< MeshFilter >().mesh = wa.waterPlane;
         w.parent = pools.transform;
         c++;
         if (c == s.numPools) { break; }
      }
      print("water generation time: " + (Time.realtimeSinceStartup - startTime) + " seconds");

      List< float > altValues   = new List< float >();
      List< float > slopeValues = new List< float >();
      int count   = 0;

      // send height and slope values to texture generator
      foreach (Tile t in tilesByPos.Values) {

         t.GenerateMesh();
         if (t.outside || t.underwater) { continue; }
         altValues.Add( t.height );
         slopeValues.Add( t.slope );
         count++;
      }
      texGen.CalcDistributions( altValues.ToArray(), slopeValues.ToArray() );

      // assign texture
      foreach (Tile t in tilesByPos.Values) {
         //print( "slope: " + t.slope );
         t.CalculateNormals();
         t.SetMaterial( texGen.GetMaterial( t.textureHeight, t.slope, !t.outside ) );
      }
      texGen.ShowStats();

      // position sound units
      /*Vector3[] positions = {
      
         new Vector3( s.borderWidth + soundPosition, 100.0f, s.borderWidth + soundPosition ),
         new Vector3( s.borderWidth + soundPosition, 100.0f, s.mapSize - s.borderWidth - soundPosition ),
         new Vector3( s.mapSize - s.borderWidth - soundPosition, 100.0f, s.borderWidth + soundPosition ),
         new Vector3( s.mapSize - s.borderWidth - soundPosition, 100.0f, s.mapSize - s.borderWidth - soundPosition )
      };
   
      for (int i = 0; i < soundUnits.Length; i++) {

         RaycastHit hit;
         Physics.Raycast( positions[i], Vector3.down, out hit );
         soundUnits[i].transform.position = hit.point;
      }*/

      AstarPath path  = GetComponent< AstarPath >();
      pathGrid        = path.astarData.gridGraph;
      int numNodes    = (int)((s.mapSize - s.borderWidth*2) / pathGrid.nodeSize);
      pathGrid.center = new Vector3( s.mapSize / 2.0f, 0.0f, s.mapSize / 2.0f );
      pathGrid.width  = numNodes;
      pathGrid.depth  = numNodes;
      pathGrid.UpdateSizeFromWidthDepth();
      path.Scan();

      // position enemies
      for (int i = 0; i < numEnemies && !disableEnemies; i++) {

         Tile tile = GetRandomUnoccupiedTile();
         Transform enemy = Instantiate( enemies[ Random.Range( 0, enemies.Length ) ],
            tile.transform.position + Vector3.up * (tile.height + 1.0f), Quaternion.identity ) as Transform;
         enemyInstances.Add( enemy.GetComponent< Enemy >() );
      }
   }

   public static void TileChanged( Bounds bounds ) {

      if (updateArea == null) { updateArea = bounds; }
      else {
         //print("Adding to update bounds: " + bounds.max.y);
         Bounds ua = new Bounds();
         ua.SetMinMax( Vector3.Min( updateArea.Value.min, bounds.min - Vector3.one * -1.0f ),
                       Vector3.Max( updateArea.Value.max, bounds.max + Vector3.one *  1.0f ) );
         //print("Bounds top is now: " + ua.max.y);
         updateArea = ua;
      }
      if (!pathUpdate.on && !pathUpdate.stopping) { pathUpdate.Start(); }
   }

   public static Tile GetRandomUnoccupiedTile() {

      Tile tile = null;
      while (!tile || tile.outside || tile.occupied) { tile = tileList[ Random.Range( 0, tileList.Count ) ]; }
      return tile;
   }

   public static Tile[] GetTiles( Hex[] hexes ) {

      return hexes.Select( h => tilesByHex[ h ] ).ToArray();
   }

   void Update() {

      if (pathUpdate.stopping && updateArea != null) {

         boundsIndicator.position   = updateArea.Value.center;
         boundsIndicator.localScale = updateArea.Value.extents * 2.0f;
         updateArea.Value.SetMinMax( updateArea.Value.min, updateArea.Value.max + Vector3.up * 2.0f );
         AstarPath.active.UpdateGraphs( updateArea.Value );
         foreach (Enemy enemy in enemyInstances) { enemy.NodeUpdate( updateArea.Value ); }
         updateArea = null;
      }

      if (Input.GetKey("escape")) { Application.Quit(); }
   }

}
