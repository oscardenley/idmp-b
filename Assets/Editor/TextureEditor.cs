﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TextureGenerator))]
public class TextureEditor : Editor {

public override void OnInspectorGUI() {

   if (GUILayout.Button("Generate Textures")) { ((TextureGenerator)target).GenerateTextures(); }

   DrawDefaultInspector();
}

void Start() {
	
}
	
void Update() {
	
}

}
